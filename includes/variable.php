<?php
    // Time zone
    date_default_timezone_set('Asia/Manila');

    // URL root
    $root = $_SERVER["DOCUMENT_ROOT"] . '/';
    // $url = 'lfch-digital.com/';
    $url = 'local.lfch-digital.com:8890/';

    // Database
    $host = 'localhost';
    $dbusername = 'root';
    // $dbpassword = 'mysql';
    $dbpassword = 'root';
    $dbname = 'db_lfchdigital';
    $con = mysqli_connect($host, $dbusername, $dbpassword, $dbname);

    // Branding
    $primaryColor = '#109d58';
    $lighterPrimaryColor = '#30bd78';
    $darkerPrimaryColor = '#209d58';
    $red = '#c54343';
    $primaryFont = '"Lato", sans-serif';
    $copyright = date("Y") . ' © Franz Lopez';

    // Elements
    $blockerBG = 'rgba(0, 0, 0, 0.35)';
    $borderRadius = '5px';
    $bodyFontSize = '15px';
?>
