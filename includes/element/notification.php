<div class="notification-container transitionize-fast">
    <div class="notification-list transitionize-fast">
        <div class="title">
            <span>Notifications</span>
            <i class="far fa-times-circle" data-close="notification"></i>
        </div>
        <div class="list"></div>
    </div>

    <span class="close-notification" data-close="notification"></span>

    <audio id="notification-audio" class="hidden" controls>
        <source src="public/system/audio/notification.ogg" type="audio/ogg">
        <source src="public/system/audio/notification.mp3" type="audio/mpeg">
        Your browser does not support the audio element.
    </audio>

    <audio id="message-audio" class="hidden" controls>
        <source src="public/system/audio/message.ogg" type="audio/ogg">
        <source src="public/system/audio/message.mp3" type="audio/mpeg">
        Your browser does not support the audio element.
    </audio>
</div>

<style>
.notification-container {
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	background-color: rgba(0, 0, 0, 0);
	z-index: 100;
	left: 0;
    pointer-events: none;
}

.notification-container.active {
	opacity: 1;
	background-color: rgba(0, 0, 0, 0.3);
    pointer-events: all;
}

.notification-list {
	width: 100%;
	max-width: 380px;
	position: absolute;
	right: 0;
	top: 0;
	bottom: 0;
	background-color: #242930;
	z-index: 2;
    color: #fff;
	transform: translateX(200%);
}

.notification-list .list {
	height: calc(100% - 50px);
	overflow-x: hidden;
	overflow-y: auto;
}

.notification-container.active .notification-list {
    transform: translateX(0);
}

.close-notification {
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1;
}

.notification-list .title {
	display: block;
	padding: 16px 25px;
}

.notification-list .title > [data-icon] {
	float: right;
	opacity: 0.6;
	position: relative;
	top: 4px;
    cursor: pointer;
}

.notification-list .title > span {
	font-size: 1.2em;
}

[data-type="notification"] {
	display: block;
	color: #fff;
	padding: 20px 25px;
	background-color: rgba(255, 255, 255, 0.05);
	border-bottom: 1px solid rgba(255, 255, 255, 0.05);
}

[data-type="notification"]:last-child {
	border-bottom: 0 none;
}

[data-type="notification"]:hover {
	background-color: rgba(255, 255, 255, 0.1);
}

[data-type="notification"] .image {
	display: inline-block;
	width: 50px;
	height: 50px;
	float: left;
	border-radius: 100%;
	margin-right: 10px;
	background-size: cover;
	background-repeat: no-repeat;
}

[data-type="notification"] .content {
	display: block;
	margin-bottom: 5px;
	margin-top: 5px;
	padding-left: 60px;
}

[data-type="notification"] .date {
	display: block;
	font-size: 0.8em;
	opacity: 0.5;
	padding-left: 60px;
}
</style>

<script>
    setTimeout(function(){
    	// get notification total
        var atype = $('.user-info').attr('atype');

        $.get(global.root + 'includes/query/notification/total.php?target=' + atype, function(data){
            localStorage.setItem('pulledNotif', data);
        });

        setTimeout(function(){
        	// get unread notification total
            global.getUnreadNotifCount();

            // fetch notifications
            global.reloadNotification();
        }, 500);
    }, 500);
</script>
