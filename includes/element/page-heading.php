<div class="page-heading">
    <span class="title">Loading ...</span>
</div>

<style>
.page-heading {
	background-color: <?=$primaryColor?>;
	background-image: linear-gradient(141deg, <?=$lighterPrimaryColor?> 0%, <?=$primaryColor?> 51%, <?=$darkerPrimaryColor?> 75%);
	padding: 85px 25px 30px 275px;
	color: #fff;
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	z-index: 10;
}

.page-heading > .title {
	font-size: 1.2em;
}

@media only screen and (max-width: 720px) {
    .page-heading > .title {
    	font-size: 1em;
    	font-weight: 600;
    	width: calc(100% - 150px);
    	display: block;
    	text-overflow: ellipsis;
    	overflow: hidden;
    	white-space: nowrap;
    }
}

.page-heading .button {
	color: #fff;
	text-decoration: none;
	background-color: rgba(0, 0, 0, 0.1);
	border-radius: 5px;
	padding: 10px 15px;
	top: 76px;
	right: 25px;
	position: absolute;
}

.page-heading .button:hover {
    background-color: rgba(0, 0, 0, 0.2);
}
</style>
