<div class="side-panel">
    <div class="user-info">
        <div class="image"></div>
        <h1 class="name"></h1>
        <span class="title"></span>
    </div>

    <a class="panel-item" href="#" pin="dashboard"><i class="far fa-home"></i><span>Dashboard</span></a>
    <a class="panel-item" href="#" pin="messages"><i class="far fa-envelope"></i><span>Messages</span></a>
    <a class="panel-item" href="#" pin="patients"><i class="far fa-user-friends"></i><span>Patients</span></a>
    <a class="panel-item child" href="#" pin="add-patient"><i class="far fa-user-plus"></i><span>Add Patient</span></a>
    <a class="panel-item child hidden" href="#" pin="patient-profile"><span>Patient Profile</span></a>
    <a class="panel-item child hidden" href="#" pin="edit-patient"><span>Edit Patient</span></a>
    <a class="panel-item" href="#" pin="schedules"><i class="far fa-clock"></i><span>Schedules</span></a>
    <a class="panel-item logout"><i class="far fa-sign-out"></i><span>Logout</span></a>
</div>


<script type="text/javascript">
    if (!$('body').hasClass('side-panel-functions-loaded')) {
        $('body').addClass('side-panel-functions-loaded');

        $(window).resize(function() {
            global.setSidePanelSize();
        });
    }

    global.setSidePanelSize();

    var pID = <?php echo $_SESSION['id'] ?>;
    $('.user-info').attr('uid', pID);


    $(document).on('click', '.header [title="Toggle Menu"]', function() {
        if ($('body').attr('panel-open') == 'true') {
            $('body').attr('panel-open', false);
        } else {
            $('body').attr('panel-open', true);
        }
    });


    setTimeout(function(){
        $.get(global.root + 'includes/query/user/view.php?id=' + pID, function(data){
            $('.user-info .image').css('background-image', 'url("' + global.root + 'public/system/images/avatar/' + data[0].image + '.jpg")');
            $('.user-info .title').text(data[0].title);
            $('.user-info').attr('atype', data[0].atype);

            if (data[0].atype == 'admin') {
                $('.user-info .name').text('Dr. ' + data[0].fname + ' ' + data[0].lname);
            } else {
                $('.user-info .name').text(data[0].fname + ' ' + data[0].lname);
            }
        });
    }, 100);
</script>
