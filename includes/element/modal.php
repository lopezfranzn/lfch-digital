<div class="modal-container transitionize-faster">
    <div class="modal transitionize-faster">
        <div class="container">
            <h1 class="title"></h1>
            <div class="content"></div>
            <div class="action"></div>
        </div>
    </div>
</div>

<style>
.modal-container {
	position: fixed;
	z-index: 1000;
	background-color: <?=$blockerBG?>;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
    opacity: 0;
    pointer-events: none;
}

.modal-container.active {
    opacity: 1;
    pointer-events: auto;
}

.modal-container .modal {
	position: absolute;
	top: 50%;
	left: 50%;
    width: 100%;
	background-color: #fff;
	box-shadow: 10px 10px 20px -10px rgba(0, 0, 0, 0.5);
	border-radius: 10px;
    transform: translate(-50%, -50%) scale(0);
}

.modal-container.active .modal {
    transform: translate(-50%, -50%) scale(1);
}

.modal .title {
	margin: 0;
	padding: 20px 25px 20px 25px;
	font-size: 1.4em;
	opacity: 0.8;
}

.modal .content {
	padding: 0 25px;
	margin-bottom: 10px;
}

.modal .content > span {
	opacity: 0.5;
}

.modal .action {
	text-align: right;
	padding-right: 15px;
}

.modal .action .button {
	padding: 10px 8px;
	display: inline-block;
	margin: 10px 0px;
	text-transform: uppercase;
	font-weight: 600;
	text-decoration: none;
	color: <?=$primaryColor?>;
}
</style>
