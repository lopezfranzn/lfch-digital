<div class="selection-list closable data-type required date-month">
    <span><span class="main-value">Select Month</span><i class="far fa-angle-down"></i></span>
    <img class="carret" />
    <div class="list" style="width: 100px; height: 140px;">
        <div class="overflow">
            <span data-value="01">January</span>
            <span data-value="02" less="2">February</span>
            <span data-value="03">March</span>
            <span data-value="04" less="1">April</span>
            <span data-value="05">May</span>
            <span data-value="06" less="1">June</span>
            <span data-value="07">July</span>
            <span data-value="08">August</span>
            <span data-value="09" less="1">September</span>
            <span data-value="10">October</span>
            <span data-value="11" less="1">November</span>
            <span data-value="12">December</span>
        </div>
    </div>
</div>

<div class="selection-list closable data-type required date-day">
    <span><span class="main-value">Select Day</span><i class="far fa-angle-down"></i></span>
    <img class="carret" />
    <div class="list" style="width: 100px; height: 140px;">
        <div class="overflow"></div>
    </div>
</div>

<div class="selection-list closable data-type required date-year">
    <span><span class="main-value">Select Year</span><i class="far fa-angle-down"></i></span>
    <img class="carret" />
    <div class="list" style="width: 100px; height: 140px;">
        <div class="overflow"></div>
    </div>
</div>

<a href="#" class="clear-field"><i class="far fa-eraser"></i> Clear</a>

<div class="clear"></div>
