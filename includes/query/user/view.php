<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $id = isset($_GET['id']) ? $_GET['id'] : '';
    $limit = isset($_GET['limit']) ? $_GET['limit'] : '';
    $pageOffset = isset($_GET['offset']) ? $_GET['offset'] : '';

    $query = "SELECT DISTINCT id, username, fname, lname, title, image, email, atype, trn_date FROM `users`";

    // SYNTAX: /includes/query/user/view.php?id=1
    if ($id) {
    	$query .= " WHERE id='$id'";
    }

    // SYNTAX: /includes/query/user/view.php?limit=5&offset=5
    if ($limit) {
    	$query .= " ORDER BY id DESC LIMIT $limit OFFSET $pageOffset";
    } else {
    	$query .= " ORDER BY id DESC";
    }

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
    header("Content-type:application/json");
?>
