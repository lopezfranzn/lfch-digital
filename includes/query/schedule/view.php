<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $dateString = $_GET['date'];

    // SYNTAX: /includes/query/schedule/view.php?date=2018-03-04
    $query = "SELECT DISTINCT id, lname, fname, mname, contact_no, follow_up, follow_up_status
        FROM `patients`
        WHERE follow_up LIKE '%$dateString%'
        ORDER BY id DESC";

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
    header("Content-type:application/json");
?>
