<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $dateString = $_GET['date'];

    // SYNTAX: /includes/query/schedule/delete.php?date=2018-12-13
    $query = "DELETE FROM `notifications` WHERE content LIKE '%$dateString%'";

    if ($con->query($query) === TRUE) {
        echo 'true';
    } else {
        echo "Error deleting record: " . $con->error;
    }

    $con->close();
?>
