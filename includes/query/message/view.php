<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $mto = isset($_GET['mto']) ? $_GET['mto'] : '';

    $query = "SELECT DISTINCT id, mfrom, mto, content, status, trn_date FROM `messages`";

    // SYNTAX: /includes/query/message/view.php?mto=1
    if ($mto) {
    	$query .= " WHERE id IN (SELECT MAX(id) FROM `messages` WHERE mto='$mto' GROUP BY mfrom)";
    }

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
    header("Content-type:application/json");
?>
