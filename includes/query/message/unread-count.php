<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $mto = isset($_GET['mto']) ? $_GET['mto'] : '';

    // SYNTAX: /includes/query/message/unread-count.php?mto=1
    $query = "SELECT * FROM `messages` WHERE status='unread' AND mto='$mto'";
    $result = mysqli_query($con,$query) or die(mysql_error());
    $num_rows = mysqli_num_rows($result);

    $con->close();

    echo json_encode($num_rows);
    header("Content-type:application/json");
?>
