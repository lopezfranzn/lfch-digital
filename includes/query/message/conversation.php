<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $mfrom = isset($_GET['mfrom']) ? $_GET['mfrom'] : '';
    $mto = isset($_GET['mto']) ? $_GET['mto'] : '';
    $limit = isset($_GET['limit']) ? $_GET['limit'] : '';
    $pageOffset = isset($_GET['offset']) ? $_GET['offset'] : '';

    $query = "SELECT * FROM `messages`";

    // SYNTAX: /includes/query/message/conversation.php?mfrom=3&mto=2
    if ($mfrom) {
    	$query .= " WHERE mfrom='$mfrom' AND mto='$mto' OR mfrom='$mto' AND mto='$mfrom' ORDER BY id DESC LIMIT 10 OFFSET 0";
    }

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
    header("Content-type:application/json");
?>
