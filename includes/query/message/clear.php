<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $mfrom = isset($_GET['mfrom']) ? $_GET['mfrom'] : '';
    $mto = isset($_GET['mto']) ? $_GET['mto'] : '';

    // SYNTAX: /includes/query/message/clear.php?mfrom=2&mto=3
    $query = "UPDATE `messages` SET status='read' WHERE status='unread' AND mfrom='$mfrom' AND mto='$mto'";

    if ($con->query($query) === TRUE) {
        echo "true";
    } else {
        echo "Error updating record: " . $con->error;
    }

    $con->close();
?>
