<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $id = isset($_GET['id']) ? $_GET['id'] : '';
    $limit = isset($_GET['limit']) ? $_GET['limit'] : '';
    $pageOffset = isset($_GET['offset']) ? $_GET['offset'] : '';
    $orderBy = isset($_GET['order']) ? $_GET['order'] : '';
    $sortBy = isset($_GET['sort']) ? $_GET['sort'] : '';

    $query = "SELECT DISTINCT";

    // SYNTAX: /includes/query/patient/view.php?id=1
    if ($id) {
    	$query .= " id, image, lname, fname, mname, birth_date, contact_no, address, past_med_history, last_mens_history, age_gestation, ob_history, chief_complaint, physical_exam, lab_result, assessment, plan, follow_up, follow_up_status, trn_date FROM `patients` WHERE id='$id'";
    } else {
    	$query .= " id, image, lname, fname, mname, birth_date, address FROM `patients`";
    }

    // SYNTAX: /includes/query/patient/view.php?limit=5&offset=5
    if ($limit) {
    	$query .= " ORDER BY $orderBy $sortBy LIMIT $limit OFFSET $pageOffset";
    } else {
    	$query .= " ORDER BY id";
    }

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
    // header("Content-type:application/json");
?>
