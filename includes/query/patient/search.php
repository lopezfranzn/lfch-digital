<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $textString = isset($_GET['text']) ? $_GET['text'] : '';

    $query = "SELECT DISTINCT id, image, lname, fname, mname, birth_date, address
        FROM `patients`
        WHERE CONCAT( fname,  ' ', lname ) LIKE '%$textString%'
        OR CONCAT( fname,  ' ', mname ) LIKE '%$textString%'
        OR CONCAT( lname,  ' ', fname ) LIKE '%$textString%'
        OR CONCAT( lname,  ' ', mname ) LIKE '%$textString%'
        OR CONCAT( mname,  ' ', lname ) LIKE '%$textString%'
        OR CONCAT( mname,  ' ', fname ) LIKE '%$textString%'
        OR CONCAT( fname,  ' ', lname,  ' ', mname ) LIKE '%$textString%'
        OR CONCAT( fname,  ' ', mname,  ' ', lname ) LIKE '%$textString%'
        OR CONCAT( lname,  ' ', mname,  ' ', fname ) LIKE '%$textString%'
        OR CONCAT( lname,  ' ', fname,  ' ', mname ) LIKE '%$textString%'
        OR CONCAT( mname,  ' ', fname,  ' ', lname ) LIKE '%$textString%'
        OR CONCAT( mname,  ' ', lname,  ' ', fname ) LIKE '%$textString%'
        ORDER BY id DESC LIMIT 10";

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
    header("Content-type:application/json");
?>
