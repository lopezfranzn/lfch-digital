<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    // auto-generated data
    $image = $_GET['image'];
    // full name
    $lname = $_GET['lname'];
    $fname = $_GET['fname'];
    $mname = $_GET['mname'];
    // birth date
    $birth_date = $_GET['birth_date'];
    // contact information
    $contact_no = $_GET['contact_no'];
    $address = $_GET['address'];
    // past medical history
    $past_med_history = $_GET['past_med_history'];
    // last menstrual history
    $last_mens_history = $_GET['last_mens_history'];
    // age of gestation
    $age_gestation = $_GET['age_gestation'];
    // ob history
    $ob_history = $_GET['ob_history'];
    // chief complaint
    $chief_complaint = $_GET['chief_complaint'];
    // physical exam
    $physical_exam = 'null';
    // lab results
    $lab_result = 'null';
    // assessment
    $assessment = 'null';
    // plan
    $plan = $_GET['plan'];
    // follow up
    $follow_up = $_GET['follow_up'];
    // date today
    $trn_date = $_GET['trn_date'];

    // SYNTAX: /includes/query/patient/add.php?fname=dory&lname=boom&mname=nyer&trn_date=2018-10-10
    $query = "INSERT into `patients` (image, lname, fname, mname, birth_date, contact_no, address, past_med_history, last_mens_history, age_gestation, ob_history, chief_complaint, physical_exam, lab_result, assessment, plan, follow_up, follow_up_status, trn_date)
            VALUES ('$image', '$lname', '$fname', '$mname', '$birth_date', '$contact_no', '$address', '$past_med_history', '$last_mens_history', '$age_gestation', '$ob_history', '$chief_complaint', '$physical_exam', '$lab_result', '$assessment', '$plan', '$follow_up', 'false', '$trn_date')";

    if ($con->query($query) === TRUE) {
        echo "true";
    } else {
        echo "Error adding record: " . $con->error;
    }

    $con->close();
?>
