<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';
    // patient id
    $id = $_GET['id'];

    // auto-generated data
    $image = $_GET['image'];
    // full name
    $lname = $_GET['lname'];
    $fname = $_GET['fname'];
    $mname = $_GET['mname'];
    // birth date
    $birth_date = $_GET['birth_date'];
    // contact information
    $contact_no = $_GET['contact_no'];
    $address = $_GET['address'];
    // past medical history
    $past_med_history = $_GET['past_med_history'];
    // last menstrual history
    $last_mens_history = $_GET['last_mens_history'];
    // age of gestation
    $age_gestation = $_GET['age_gestation'];
    // ob history
    $ob_history = $_GET['ob_history'];
    // chief complaint
    $chief_complaint = $_GET['chief_complaint'];
    // physical exam
    $physical_exam = 'null';
    // lab results
    $lab_result = 'null';
    // assessment
    $assessment = 'null';
    // plan
    $plan = $_GET['plan'];
    // follow up
    $follow_up = $_GET['follow_up'];
    // date today
    $trn_date = $_GET['trn_date'];

    // SYNTAX: /includes/query/patient/edit.php?fname=dory&lname=boom&mname=nyer&trn_date=2018-10-10
    $query = "UPDATE `patients` SET
        image='$image',
        lname='$lname',
        fname='$fname',
        mname='$mname',
        birth_date='$birth_date',
        contact_no='$contact_no',
        address='$address',
        past_med_history='$past_med_history',
        last_mens_history='$last_mens_history',
        age_gestation='$age_gestation',
        ob_history='$ob_history',
        chief_complaint='$chief_complaint',
        physical_exam='$physical_exam',
        lab_result='$lab_result',
        assessment='$assessment',
        plan='$plan',
        follow_up='$follow_up',
        trn_date='$trn_date' WHERE id='$id'";


    if ($con->query($query) === TRUE) {
        echo "true";
    } else {
        echo "Error editing record: " . $con->error;
    }

    $con->close();
?>
