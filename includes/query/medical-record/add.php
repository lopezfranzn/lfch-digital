<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $pid = isset($_GET['pid']) ? $_GET['pid'] : '';
    $rec_type = isset($_GET['rtype']) ? $_GET['rtype'] : '';
    $content = isset($_GET['content']) ? $_GET['content'] : '';
    $trn_date = isset($_GET['stamp']) ? $_GET['stamp'] : '';

    // SYNTAX: /includes/query/medical-record/add.php?pid=1&rtype=pe&content=[{"bp":"120/90","cr":"10.4","temp":"38.5","abdomen":{"fh":"2","fht":"1"},"ie":"2","utz":"3","others":"Sample test"}]&stamp=2019-01-01
    $query = "INSERT into `med_records` (pid, rec_type, content, trn_date) VALUES ('$pid', '$rec_type', '$content', '$trn_date')";

    if ($con->query($query) === TRUE) {
        echo "true";
    } else {
        echo "Error adding record: " . $con->error;
    }

    $con->close();
?>
