<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $id = isset($_GET['id']) ? $_GET['id'] : '';

    // SYNTAX: /includes/query/medical-record/delete.php?id=1
    $query = "DELETE FROM `med_records` WHERE id='$id'";

    if ($con->query($query) === TRUE) {
        echo "true";
    } else {
        echo "Error deleting record: " . $con->error;
    }

    $con->close();
?>
