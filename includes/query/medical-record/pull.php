<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $pid = isset($_GET['pid']) ? $_GET['pid'] : '';
    $recordType = isset($_GET['rtype']) ? $_GET['rtype'] : '';
    $limit = isset($_GET['limit']) ? $_GET['limit'] : '';
    $pageOffset = isset($_GET['offset']) ? $_GET['offset'] : '';

    // SYNTAX: /includes/query/medical-record/pull.php?pid=1&rtype=pe&limit=10&offset=0
    $query = "SELECT * FROM `med_records` WHERE pid='$pid' AND rec_type='$recordType' ORDER BY id DESC LIMIT $limit OFFSET $pageOffset";

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
?>
