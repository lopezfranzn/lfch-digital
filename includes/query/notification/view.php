<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $target = isset($_GET['target']) ? $_GET['target'] : '';

    $query = "SELECT DISTINCT id, nfrom, target, content, status, trn_date FROM `notifications`";

    // SYNTAX: /includes/query/notification/view.php?target=admin
    if ($target) {
    	$query .= " WHERE target='$target' ORDER BY id DESC LIMIT 20 OFFSET 0";
    }

    $result = mysqli_query($con,$query) or die(mysql_error());
    $rows = array();

    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    $con->close();

    echo json_encode($rows);
    header("Content-type:application/json");
?>
