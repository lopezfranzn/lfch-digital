<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    $nfrom = isset($_GET['nfrom']) ? $_GET['nfrom'] : '';
    $target = isset($_GET['target']) ? $_GET['target'] : '';
    $content = isset($_GET['content']) ? $_GET['content'] : '';
    $trn_date = date('F j, Y g:i A  ');

    // SYNTAX: /includes/query/notification/add.php?nfrom=1&target=admin&content=contenthere
    $query = "INSERT into `notifications` (nfrom, target, content, status, trn_date) VALUES ('$nfrom', '$target', '$content', 'unread', '$trn_date')";

    if ($con->query($query) === TRUE) {
        echo "true";
    } else {
        echo "Error adding record: " . $con->error;
    }

    $con->close();
?>
