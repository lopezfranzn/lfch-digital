<?php
    include '../../../includes/auth.php';
    include '../../../includes/db.php';

    // SYNTAX: /includes/query/notification/clear.php
    $query = "UPDATE `notifications` SET status='read' WHERE status='unread'";

    if ($con->query($query) === TRUE) {
        echo "true";
    } else {
        echo "Error updating record: " . $con->error;
    }

    $con->close();
?>
