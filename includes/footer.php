    <div class="credit">
        <span><?=$copyright?></span>
    </div>

    <script type="text/javascript">
        if (!$('body').hasClass('login-page')) {
            if (!localStorage.getItem('audioAllowedDashboard')) {
                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['allowAudio']
                    global.openModal(md.title, md.content, md.button, md.size);
                });
            }
        }

        $(window).on('load', function() {
            $('body').removeClass('hidden');
            console.log('System Loading Complete');
        });
    </script>

    <?php include $root . 'public/style/style.php'; ?>

</body>
</html>
