<?php
    include $_SERVER["DOCUMENT_ROOT"] . '/includes/variable.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loading ...</title>
    <link href="/public/system/images/favicon.png" rel="shortcut icon">

    <!-- SCRIPTS -->
    <!-- Plugins -->
    <script type="text/javascript" src="public/script/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="public/script/plugins/fontawesome-all.min.js"></script>
    <script type="text/javascript" src="public/script/plugins/masonry.js"></script>

    <!-- Global -->
    <script type="text/javascript" src="public/script/global.js"></script>

    <!-- Main -->
    <script type="text/javascript">
        // set root directory
        if (localStorage.getItem('root') != 'https://' + '<?=$url?>') {
            localStorage.setItem('root', 'https://' + '<?=$url?>');
        }
    </script>
    <script type="text/javascript" src="public/script/main.js"></script>

    <!-- Element -->
    <script type="text/javascript" src="public/script/element/modal.js"></script>
    <script type="text/javascript" src="public/script/element/date-picker.js"></script>
    <script type="text/javascript" src="public/script/element/checkbox.js"></script>
    <script type="text/javascript" src="public/script/element/option-box.js"></script>
    <script type="text/javascript" src="public/script/element/item-list.js"></script>

    <!-- Patient -->
    <script type="text/javascript" src="public/script/patient/add.js"></script>
    <script type="text/javascript" src="public/script/patient/delete.js"></script>
    <script type="text/javascript" src="public/script/patient/view.js"></script>
    <script type="text/javascript" src="public/script/patient/edit.js"></script>
    <script type="text/javascript" src="public/script/patient/search.js"></script>

    <!-- Message -->
    <script type="text/javascript" src="public/script/message/view.js"></script>
    <script type="text/javascript" src="public/script/message/add.js"></script>
    <script type="text/javascript" src="public/script/message/pull.js"></script>

    <!-- Notification -->
    <script type="text/javascript" src="public/script/notification/toggle.js"></script>
    <script type="text/javascript" src="public/script/notification/pull.js"></script>
    <script type="text/javascript" src="public/script/notification/view.js"></script>

    <!-- Schedule -->
    <script type="text/javascript" src="public/script/schedules/pull.js"></script>

    <!-- Medical Record -->
    <script type="text/javascript" src="public/script/medical-record/pull.js"></script>
    <script type="text/javascript" src="public/script/medical-record/delete.js"></script>
</head>

<body panel-open="true" class="hidden">
    <?php include $root . 'includes/header.php'; ?>
