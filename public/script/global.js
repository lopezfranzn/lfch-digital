class globalFunctionalities {
    // global variables
    constructor() {
        this.appName = 'LFCH';
        this.root = localStorage.getItem('root');
        this.newDate = new Date();
        this.month = this.newDate.getMonth()+1;
        this.day = this.newDate.getDate();
        this.fulldate = this.newDate.getFullYear() + '-' +
            (this.month<10 ? '0' : '') + this.month + '-' +
            (this.day<10 ? '0' : '') + this.day;
        this.patientViewLimit;
        this.patientViewOffset;
        this.mainPage = 'dashboard';
        this.adminMail = 'info.lfch@gmail.com';
        this.noData = '<span display="true">No data to show</span>';
        this.peTotal;
        this.lrTotal;
        this.atTotal;
        this.medRecResultLimit = 10;
    }

    // set start information
    setInfo(pname, header) {
        var dashedCasedTitle = pname.replace(/\s+/g, '-').toLowerCase();
        var properCasedTitle = pname.toString().replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() +
                               txt.substr(1).toLowerCase();});
        localStorage.setItem('currentPage', dashedCasedTitle);

        $('title').text(properCasedTitle + ' - ' + global.appName);
    	$('body').addClass(dashedCasedTitle + '-page');

        if (header == true) {
        	$('.header').removeClass('hidden');
        } else {
        	$('.header').addClass('hidden');
        }

    	$('.panel-item').removeClass('active');
    	$('.panel-item[pin="' + pname + '"]').addClass('active');

        $('.page-heading .button').remove();
        $('.page-heading > .title').text(properCasedTitle);
    }

    // go to main page
    goToMain() {
        var currentPage = localStorage.getItem('currentPage');

        if (currentPage == null) {
            $('.panel-item[pin="' + global.mainPage + '"]').click();
        } else {
            $('.panel-item[pin="' + currentPage + '"]').click();
        }

        setTimeout(function(){
            if ($('.page-heading > .title').text() == 'Loading ...') {
                global.goToMain();
            }
        }, 1000);
    }

    // open modal
    openModal(title, content, button, size) {
        if (title != null) {
            $('.modal-container .title').html(title);
        }

        if (content != null) {
            $('.modal-container .content').html(content);
        }

        if (button != null) {
            $('.modal-container .action').html(button);
        }

        if (size != null) {
            $('.modal-container .modal').css('max-width', size);
        } else {
            $('.modal-container .modal').css('max-width', '500px');
        }

        setTimeout(function(){
            $('.modal-container').addClass('active');
        }, 100);
    }

    // get date with format
    getDate(separator, format) {
        var dYear = global.newDate.getFullYear();
        var dMonth = (global.month<10 ? '0' : '') + global.month;
        var dDay = (global.day<10 ? '0' : '') + global.day;
        var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        var fullDate = new Date();

        if (format == 'mdy') {
            return dMonth + separator + dDay + separator + dYear;
        } else if (format == 'ydm') {
            return dYear + separator + dDay + separator + dMonth;
        } else if (format == 'ymd') {
            return dYear + separator + dMonth + separator + dDay;
        } else if (format == 'dmy') {
            return dDay + separator + dMonth + separator + dYear;
        } else if (format == 'fmdy') {
            return monthNames[fullDate.getMonth()] + ' ' + dDay + ', ' + dYear;
        } else if (format == 'dfm') {
            return dDay + ' ' + monthNames[fullDate.getMonth()];
        }
    }

    // get week day name
    getWeekDay() {
        var weekday=new Array(7);
        weekday[0]="Sunday";
        weekday[1]="Monday";
        weekday[2]="Tuesday";
        weekday[3]="Wednesday";
        weekday[4]="Thursday";
        weekday[5]="Friday";
        weekday[6]="Saturday";

        var d = new Date();
        return weekday[d.getDay()];
    }

    // load person list in messages
    loadPersonData(mfrom, mto, excerpt, status) {
        if ($('body').hasClass('messages-page')) {
            $.ajaxSetup({async:false});

            var uID = $('.user-info').attr('uid');
            if (mfrom == uID) {
                $.get(global.root + 'includes/query/user/view.php?id=' + mto, function(data){
                    if (data[0].id != $('.people-list .list .person[uid="' + data[0].id + '"]').attr('uid')) {
                        $('.people-list .list').prepend('<a href="#" class="person" status="' + status + '" uid="' + data[0].id + '">' +
                            '<span class="image" style="background-image: url(' + global.root + 'public/system/images/avatar/' + data[0].image + '.jpg)"></span>' +
                            '<span class="name"></span><span class="excerpt">' + excerpt + '</span><i class="far fa-angle-right"></i><div class="clear"></div>'
                        + '</a>');

                        if (data[0].atype == 'admin') {
                            $('.person[uid="' + data[0].id + '"] .name').text('Dr. ' + data[0].fname + ' ' + data[0].lname);
                        } else {
                            $('.person[uid="' + data[0].id + '"] .name').text(data[0].fname + ' ' + data[0].lname);
                        }
                    }
                });
            } else {
                $.get(global.root + 'includes/query/user/view.php?id=' + mfrom, function(data){
                    if (data[0].id != $('.people-list .list .person[uid="' + data[0].id + '"]').attr('uid')) {
                        $('.people-list .list').prepend('<a href="#" class="person" status="' + status + '" uid="' + data[0].id + '">' +
                            '<span class="image" style="background-image: url(' + global.root + 'public/system/images/avatar/' + data[0].image + '.jpg)"></span>' +
                            '<span class="name"></span><span class="excerpt">' + excerpt + '</span><i class="far fa-angle-right"></i><div class="clear"></div>'
                        + '</a>');

                        if (data[0].atype == 'admin') {
                            $('.person[uid="' + data[0].id + '"] .name').text('Dr. ' + data[0].fname + ' ' + data[0].lname);
                        } else {
                            $('.person[uid="' + data[0].id + '"] .name').text(data[0].fname + ' ' + data[0].lname);
                        }
                    }
                });
            }
        }
    }


    // get age
    getAge(birthDate, todayDate) {
        var arrayOfToday = todayDate.split("-");
        var tYear = arrayOfToday[0];
        var tMonth = arrayOfToday[1];
        var tDay = arrayOfToday[2];

        var arrayOfBirth = birthDate.split("-");
        var bYear = arrayOfBirth[0];
        var bMonth = arrayOfBirth[1];
        var bDay = arrayOfBirth[2];

        var yearDiff = tYear - bYear;

        if (bMonth < tMonth) {
            return yearDiff;
        } else if (bMonth == tMonth) {
            if (bDay <= tDay) {
                return yearDiff;
            } else {
                return yearDiff - 1;
            }
        } else {
            return yearDiff - 1;
        }
    }


    detailedDate(date) {
        var arrayDate = date.split("-");
        var dYear = arrayDate[0];
        var dMonth = arrayDate[1];
        var dDay = arrayDate[2];

        dMonth = parseInt(Number(dMonth).toString()) - 1;

        var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

        return monthNames[dMonth] + ' ' + dDay + ', ' + dYear;
    }


    // close all objects that can be close
    closeAllClosables(thisObj) {
        $('.closable').not(thisObj).removeClass('active');
        $('.closable .list').hide();
    }


    // load date picker
    loadDatePicker() {
        $.get(global.root + 'includes/element/date-picker.php', function(data){
            $('.date-container').each(function(i) {
                $(this).addClass('dtpicker-c' + i).html(data);
                global.outputDPDays('dtpicker-c' + i);
                global.outputDPYears('dtpicker-c' + i);
                global.closeAllClosables();
            });
        });
    }


    // output years
    outputDPYears(className) {
        var that = $('.' + className + ' .date-year');
        var maxAge = 200;
        var currentYear;

        if ($('.' + className).hasClass('advanced')) {
            currentYear = parseInt((new Date()).getFullYear()) + 1;
        } else {
            currentYear = (new Date()).getFullYear();
        }

        that.find('.overflow').html('');
        for (var i = 0; i < maxAge; i++) {
            var year =  currentYear - i;
            that.find('.overflow').append('<span data-value="' + year + '">' + year + '</span>');
        }
    }

    // output days
    outputDPDays(className) {
        var that = $('.' + className + ' .date-day');
        that.find('.overflow').html('');
        for (var i = 1; i < 32; i++) {
            if (i < 10) {
                that.find('.overflow').append('<span data-value="0' + i + '">' + i + '</span>');
            } else {
                that.find('.overflow').append('<span data-value="' + i + '">' + i + '</span>');
            }
        }
    }


    // extract date
    extractDate(date, dpClass) {
        var targetDP = $('.' + dpClass);
        var arrayOfDate = date.split("-");
        var dYear = arrayOfDate[0];
        var dMonth = arrayOfDate[1];
        var dDay = arrayOfDate[2];

        targetDP.find('.date-month').addClass('changed').find('.main-value').attr('data-value', dMonth).text(dMonth);
        targetDP.find('.date-day').addClass('changed').find('.main-value').attr('data-value', dDay).text(dDay);
        targetDP.find('.date-year').addClass('changed').find('.main-value').attr('data-value', dYear).text(dYear);
    }


    // extract list
    extractList(listItem, listClass) {
        var itemCount = listItem.length;
        var placeholders = 'Enter ' + listClass.split('-list').join("") + ' item';

        if (itemCount != 0) {
            $('.' + listClass).html('');

            for (var i = 0; i < itemCount; i++) {
                var currentCount = i + 1;
                $('.' + listClass).append('<div class="item" child="' + currentCount + '"><input type="text" placeholder="' + placeholders + '"><i class="fa fa-times"></i></div>');
                $('.' + listClass + ' .item[child="' + currentCount + '"] input').removeClass('new').val(listItem[i].replace(/SYSDQUOTE/g, '"').replace(/SYSQUOTE/g, "'"));

                if (currentCount == itemCount) {
                    $('.' + listClass + ' .item').removeAttr('child');
                    $('.' + listClass).append('<a href="#" class="add-item"><i class="fa fa-plus"></i>Add item</a>')
                }
            }
        }
    }


    // checkbox value
    checkBoxVal(checkValue, dataValue) {
        $('a[data-value="' + dataValue + '"]').attr('check', checkValue);

        if (checkValue == 'true') {
            $('a[data-value="' + dataValue + '"] > svg, a[data-value="' + dataValue + '"] > i').remove();
            $('a[data-value="' + dataValue + '"]').prepend('<i class="far fa-check-square"></i>');
            $('a[data-value="' + dataValue + '"]').next('.item-below').removeClass('hidden');
        } else {
            $('a[data-value="' + dataValue + '"] > svg, a[data-value="' + dataValue + '"] > i').remove();
            $('a[data-value="' + dataValue + '"]').prepend('<i class="far fa-square"></i>');
            $('a[data-value="' + dataValue + '"]').next('.item-below').addClass('hidden');
        }
    }


    // load number collections
    loadNumberCollections() {
        $('.numbers-collection').each(function() {
            var that = $(this);
            var limit = parseInt(that.attr('limit')) + 1;
            that.find('.overflow').html('');
            for (var i = 0; i < limit; i++) {
                if (i < 10) {
                    that.find('.overflow').append('<span data-value="0' + i + '">' + i + '</span>');
                } else {
                    that.find('.overflow').append('<span data-value="' + i + '">' + i + '</span>');
                }

                global.closeAllClosables();
            }
        });
    }


    // extract OB history digits
    extractOBHistorySpaces(numbers) {
        var arrayOfNumbers = numbers.split("-");
        var fCount1 = arrayOfNumbers[0];
        var fCount2 = arrayOfNumbers[1];
        var fCount3 = arrayOfNumbers[2];
        var fCount4 = arrayOfNumbers[3];

        $('.ob-history-spaces [field-count="1"]').addClass('changed').find('.main-value').attr('data-value', fCount1).text(fCount1);
        $('.ob-history-spaces [field-count="2"]').addClass('changed').find('.main-value').attr('data-value', fCount2).text(fCount2);
        $('.ob-history-spaces [field-count="3"]').addClass('changed').find('.main-value').attr('data-value', fCount3).text(fCount3);
        $('.ob-history-spaces [field-count="4"]').addClass('changed').find('.main-value').attr('data-value', fCount4).text(fCount4);
    }


    // load item lists
    loadItemList() {
        $('.item-list').each(function() {
            var that = $(this);
            var placeholder = that.attr('placeholders');
            var content = '<div class="item"><input type="text" placeholder="' + placeholder + '" /><i class="fa fa-times"></i></div><a href="#" class="add-item"><i class="fa fa-plus"></i>Add item</a>';
            that.html('').append(content);
        });
    }

    // output user full name
    getUserFullName(id) {
        var result;


        if ($('[name-user-id="' + id + '"]').length > 0) {
            result = $('[name-user-id="' + id + '"]:first').text();
        } else {
            $.ajax({
                url: global.root + 'includes/query/user/view.php?id=' + id,
                type: 'get',
                dataType: 'json',
                async: false,
                success: function(data) {
                    if (data[0].atype == 'admin') {
                        result = 'Dr. ' + data[0].fname + ' ' + data[0].lname;
                    } else {
                        result = data[0].fname + ' ' + data[0].lname;
                    }
                }
            });
        }

        return result;
    }

    // output user image
    getUserImage(id) {
        var result;

        if ($('[image-user-id="' + id + '"]').length > 0) {
            var bi = $('[image-user-id="' + id + '"]').css("background-image");
            result = bi.split(/"/)[1];
        } else {
            $.ajax({
                url: global.root + 'includes/query/user/view.php?id=' + id,
                type: 'get',
                dataType: 'json',
                async: false,
                success: function(data) {
                    result = global.root + 'public/system/images/avatar/' + data[0].image + '.jpg';
                }
            });
        }

        return result;
    }

    // push notification
    pushNotification(nfrom, target, content) {
        $.get(global.root + 'includes/query/notification/add.php?nfrom=' + nfrom + '&target=' + target + '&content=' + content, function(data){
            if (data != 'true') {
                console.log(data);

                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['databaseFailed']
                    global.openModal(md.title, md.content, md.button, md.size);
                });
            }
        });
    }

    // reload notification area
    reloadNotification() {
        var atype = $('.user-info').attr('atype');
        $.get(global.root + '/includes/query/notification/view.php?target=' + atype, function(data){
            if (data != null) {
                $('.notification-list .list').html('');
                for (var i = 0; i < data.length; i++) {
                    var currentCount = i + 1;

                    if (data[i].nfrom == 'system') {
                        $('.notification-list .list').append('<a href="#" pin="schedules" class="' + i + ' new" notification-id="' + data[i].id + '" data-type="notification" data-close="notification"></a>');
                        $('a[data-type="notification"].' + i + '.new').append('<div class="image" style="background-image: url(' + global.root + 'public/system/images/avatar/robot.jpg)"><div>');
                        $('a[data-type="notification"].' + i + '.new').append('<div class="content">' + data[i].content + '</div>');
                        $('a[data-type="notification"].' + i + '.new .fn-date').text('today');
                    } else {
                        $('.notification-list .list').append('<a href="#" class="' + i + ' new" notification-id="' + data[i].id + '" data-type="notification" data-close="notification"></a>');
                        $('a[data-type="notification"].' + i + '.new').append('<div class="image" image-user-id="' + data[i].nfrom + '" style="background-image: url(' + global.getUserImage(data[i].nfrom) + ')"><div>');
                        $('a[data-type="notification"].' + i + '.new').append('<div class="content"><span name-user-id="' + data[i].nfrom + '">' + global.getUserFullName(data[i].nfrom) + '</span> ' + data[i].content + '</div>');
                    }

                    var pID = $('a[data-type="notification"].' + i + '.new i').attr('target-patient');

                    if (pID != undefined) {
                        $('a[data-type="notification"].' + i + '.new').attr('patient-id', pID);
                    }

                    $('a[data-type="notification"].' + i + '.new').append('<span class="date">' + data[i].trn_date + '</span>');

                    if (currentCount == data.length) {
                        $('.notification-list a.new').removeAttr('class').addClass('view-notification');
                    }
                }
            }
        });
    }

    // get unread notification count
    getUnreadNotifCount() {
        var atype = $('.user-info').attr('atype');
        $.get(global.root + 'includes/query/notification/unread-count?target=' + atype, function(data){
            if (data != 0) {
                $('a[title="Notifications"] .counter').remove();
                $('a[title="Notifications"]').append('<span class="counter">' + data + '</span>');
            } else {
                $('a[title="Notifications"] .counter').remove();
            }
        });
    }

    // clear all unread notifications
    clearNotifications() {
        $.get(global.root + 'includes/query/notification/clear', function(data){
            if (data != 'true') {
                console.log(data);

                $.get(global.root + 'public/system/data/modal.json', function(result){
                    var md = result['databaseFailed']
                    global.openModal(md.title, md.content, md.button, md.size);
                });
            } else {
                $('a[title="Notifications"] .counter').remove();
            }
        });
    }


    // reload active page
    reloadPatientList() {
        if ($('div').hasClass('patient-list')) {
            $('.panel-item.active').click();
        }
    }

    // reload messages area
    reloadMessages() {
        if ($('body').hasClass('messages-page')) {
            var uID = $('.user-info').attr('uid');

            $.get(global.root + 'includes/query/message/view.php?mto=' + uID, function(data){
                if (data != null) {
                    $('.people-list .list').html('');
                    for (var i = 0; i < data.length; i++) {
                        var currentCount = i + 1;
                        var excerpt = data[i].content;
                        var mfrom = data[i].mfrom;
                        var mto = data[i].mto;
                        var status = data[i].status;
                        global.loadPersonData(mfrom, mto, excerpt, status);
                        if (currentCount == data.length) {
                            var convoID = localStorage.getItem('convoID');
                            if (convoID != null) {
                                setTimeout(function(){
                                    $('.people-list .list .person[uid="' + convoID + '"]').click();
                                }, 1000);
                            } else {
                                setTimeout(function(){
                                    $('.people-list .list .person:first-child').click();
                                }, 1000);
                            }
                        }
                    }
                }
            });
        }
    }

    // get unread messages count
    getUnreadMessageCount() {
        var uID = $('.user-info').attr('uid');
        $.get(global.root + 'includes/query/message/unread-count?mto=' + uID, function(data){
            if (data != 0) {
                $('.panel-item[pin="messages"] .counter').remove();
                $('.panel-item[pin="messages"]').append('<span class="counter">' + data + '</span>');
            } else {
                $('.panel-item[pin="messages"] .counter').remove();
            }
        });
    }

    // clear unread message
    clearMessages(id) {
        var uID = $('.user-info').attr('uid');
        $.get(global.root + '/includes/query/message/clear.php?mfrom=' + id + '&mto=' + uID, function(data){
            if (data != 'true') {
                console.log(data);

                $.get(global.root + 'public/system/data/modal.json', function(result){
                    var md = result['databaseFailed']
                    global.openModal(md.title, md.content, md.button, md.size);
                });
            } else {
                $('.people-list .person[uid="' + id + '"]').attr('status', 'read');
                global.getUnreadMessageCount();
            }
        });
    }

    // get list value
    getListValue(className) {
        var listCount = $('.' + className + ' > .item').length;
        var dataValue;

        for (var i = 0; i < listCount; i++) {
            var currentCount = i + 1;
            var currentValue = $('.' + className + ' > .item:nth-child(' + currentCount + ') > input').val().replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE');

            if (currentCount == 1) {
                if (listCount == 1) {
                    if (currentValue != '') {
                        dataValue = '"' + currentValue + '"';

                        return dataValue;
                    }
                } else {
                    if (currentValue != '') {
                        dataValue = '"' + currentValue + '",';
                    }
                }
            } else if (currentCount == listCount) {
                if (currentValue != '') {
                    dataValue = dataValue + '"' + currentValue + '"';
                }

                return dataValue;
            } else {
                if (currentValue != '') {
                    dataValue = dataValue + '"' + currentValue + '",';
                }
            }
        }
    }

    // get date value
    getDateValue(className) {
        if ($('.' + className + ' .date-year .main-value').attr('data-value') == undefined ||
            $('.' + className + ' .date-month .main-value').attr('data-value') == undefined ||
            $('.' + className + ' .date-day .main-value').attr('data-value') == undefined) {
            return null;
        } else {
            return $('.' + className + ' .date-year .main-value').attr('data-value')
                + '-' + $('.' + className + ' .date-month .main-value').attr('data-value')
                + '-' + $('.' + className + ' .date-day .main-value').attr('data-value');
        }
    }

    // fetch list
    fetchList(className, data) {
        var dataCount = data.length;
        for (var i = 0; i < dataCount; i++) {
            if ($('.' + className).parent('div').hasClass('values')) {
                $('.' + className).append('<li class="item"><i class="far fa-chevron-right"></i>' + data[i] + '</li>');
            } else if ($('.' + className).parent('div').hasClass('item')) {
                $('.' + className).append('<li class="item"><i class="far fa-chevron-right"></i>' + data[i] + '</li>');
            } else {
                $('.' + className).append('<li class="item"><i class="far fa-circle"></i>' + data[i] + '</li>');
            }
        }
    }


    // get patient list
    getPatientList(data) {
        if (data.length == 0) {
            $.get(global.root + 'includes/query/patient/total', function(data){
                $('.load-more-patients').html('All <strong>' + data + '</strong> Patients Loaded');
            });
        } else {
            for (var i = 0; i < data.length; i++) {
                // console.log(data[i]);
                var currentCount = i + 1;
                $('.patient-list').append('<a href="#" class="' + i + ' new" patient-id="' + data[i].id + '" data-type="patient"></a>');
                $('a.' + i + '.new').append('<div class="image" style="background-image: url(' + global.root + 'public/system/images/avatar/patients/' + data[i].image + '.jpg)"</div>');
                $('a[data-type="patient"].' + i + '.new').append('<div class="name">' + data[i].lname + ', ' + data[i].fname + ' ' + data[i].mname + '</div>');
                $('a[data-type="patient"].' + i + '.new').append('<div class="address"><i class="far fa-map-marker-alt"></i> ' + data[i].address + '</div>');

                var patientAge = global.getAge(data[i].birth_date, global.fulldate);
                if (patientAge <= 1) {
                    $('a[data-type="patient"].' + i + '.new').append('<div class="age">' + global.getAge(data[i].birth_date, global.fulldate) + ' year old</div>');
                } else {
                    $('a[data-type="patient"].' + i + '.new').append('<div class="age">' + global.getAge(data[i].birth_date, global.fulldate) + ' years old</div>');
                }

                $('a[data-type="patient"].' + i + '.new').append('<div class="functions"><a href="#" class="go"><i class="far fa-angle-right"></i></a><div>');

                if (currentCount == data.length) {
                    $('.patient-list a.new').removeAttr('class').addClass('view-patient');
                    var newTotalLoaded = parseInt(localStorage.getItem('pLoaded')) + data.length;
                    localStorage.setItem('pLoaded', newTotalLoaded);

                    $.get(global.root + 'includes/query/patient/total', function(data){
                        var totalLoaded = localStorage.getItem('pLoaded');

                        if (totalLoaded >= data) {
                            $('.load-more-patients').html('All <strong>' + data + '</strong> Patients Loaded');
                        }
                    });
                }
            }
        }
    }


    // order or sort patients
    reorderPatientList(order, sort) {
        $.get(global.root + '/includes/query/patient/view.php?order=' + order + '&sort=' + sort + '&limit=' + global.patientViewLimit + '&offset=' + global.patientViewOffset, function(data){
            if (data.length == 0 || data == '') {
                $('.load-more-patients').html('No patient to show');
            } else {
                data = JSON.parse(data);
                global.getPatientList(data);
            }
        });
    }


    // get follow up list
    getFollowUpList(date, day, dayName) {
        if ($('body').hasClass('schedules-page')) {
            $.ajaxSetup({async:false});

            function triggerFetchSchedule(date, day, dayName) {
                if ($('body').hasClass('schedules-page')) {
                    $.get(global.root + '/includes/query/schedule/view.php?date=' + date, function(data){
                        if (data.length != 0) {
                            $('.schedule-list').append('<h3 class="day-sorter"><strong>' + day + '</strong> ' + dayName + '</h3>');

                            for (var i = 0; i < data.length; i++) {
                                var currentCount = i + 1;
                                $('.schedule-list').append('<a href="#" class="' + i + ' new" patient-id="' + data[i].id + '" data-type="patient"></a>');
                                $('a[data-type="patient"].' + i + '.new').append('<span class="title">Patient follow up</span>'
                                    + '<span class="name">' + data[i].lname + ', ' + data[i].fname + ' ' + data[i].mname + '</span>'
                                    + '<span class="contact"><i class="fal fa-calendar-alt"></i>' + global.detailedDate(date) + '</span>');
                                if (data[i].follow_up_status == 'true') {
                                    $('a[data-type="patient"].' + i + '.new').append('<span class="status">Completed</span>');
                                } else {
                                    $('a[data-type="patient"].' + i + '.new').append('<span class="status not">Incomplete</span>');
                                }

                                if (currentCount == data.length) {
                                    $('.schedule-list a.new').removeAttr('class').addClass('view-patient');
                                }
                            }
                        }

                        setTimeout(()=> {
                            $('body').removeClass('single-schedule-fetching next-month-schedule-fetching');
                            clearInterval(intervalFetching);
                        }, 100);
                    });
                }
            }

            var intervalFetching = setInterval(()=> {
                if (!$('body').hasClass('single-schedule-fetching')) {
                    $('body').addClass('single-schedule-fetching');

                    triggerFetchSchedule(date, day, dayName);
                }
            }, 100);
        }
    }


    // get next month follow up list
    getNextMonthFollowUpList() {
        var intervalFetching = setInterval(()=> {
            if (!$('body').hasClass('single-schedule-fetching')) {
                $('body').addClass('next-month-schedule-fetching');

                var year = global.newDate.getFullYear();

                if (global.month == 12) {
                  global.month = 0;
                  year = year + 1;
                }

                var monthLength = global.month.toString().length;
                var dayLength = global.day.toString().length;
                var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"];

                $('.schedule-list').append('<span class="new-month">For the month of ' + monthNames[global.month] + '</span>')

                for (var i = 1; i < 32; i++) {
                    var currentCount = i + 1;
                    var newDay = i;
                    var newMonth = parseInt(global.month) + 1;

                    if (newMonth == 13) {
                      newMonth = '01';
                    }

                    var forDay = new Date(newMonth + '/' + newDay + '/' + year)
                    var weekday=new Array(7);
                    weekday[0]="Sunday";
                    weekday[1]="Monday";
                    weekday[2]="Tuesday";
                    weekday[3]="Wednesday";
                    weekday[4]="Thursday";
                    weekday[5]="Friday";
                    weekday[6]="Saturday";

                    var dayName = weekday[forDay.getDay()];

                    if (newDay.toString().length == 1) {
                        newDay = '0' + i;
                    }

                    if (newMonth.toString().length == 1) {
                        newMonth = '0' + newMonth;
                    }

                    global.getFollowUpList(year + '-' + newMonth + '-' + newDay, newDay, dayName);
                    if (currentCount == 32) {
                        setTimeout(function(){
                            if ($('body').hasClass('schedules-page')) {
                                if ($('.schedule-list > a').length == 0) {
                                    $.get(global.root + 'public/system/data/modal.json', function(result){
                                        var md = result['noFollowUp']
                                        global.openModal(md.title, md.content, md.button, md.size);
                                    });
                                }
                            }
                        }, 1000);
                    }
                }

                clearInterval(intervalFetching);
            }
        }, 1000);
    }


    // check if value is NULL
    checkIfNull(value) {
        if (value.length == 0) {
            return 'false';
        } else {
            return 'true';
        }
    }


    // set follow up status to false
    fStatusChange(id, status) {
        $.get(global.root + 'includes/query/schedule/toggle.php?id=' + id + '&status=' + status, function(data){
            if (data != 'true') {
                console.log(data);

                $.get(global.root + 'public/system/data/modal.json', function(result){
                    var md = result['databaseFailed'];
                    global.openModal(md.title, md.content, md.button, md.size);
                });
            }
        });
    }


    // push system email
    pushEmail(subject, content) {
        $.get(global.root + 'includes/mail/mailer.php?mailto=' + global.adminMail + '&subject=' + subject + '&content=' + content, function(result){
            if (result != 'true') {
                console.log('Email failed to send');
            }

            // create sql file
            setTimeout(function(){
                $.get(global.root + 'includes/database/export.php', function(){
                    setTimeout(function(){
                        // compress the file
                        $.get(global.root + 'includes/database/compress.php', function(data){
                            if (data != 'true') {
                                console.log('Failed to compress database');
                            } else {
                                setTimeout(function(){
                                    // compress the file
                                    $.get(global.root + 'includes/database/push.php', function(data){
                                        if (data != 'true') {
                                            console.log('Failed to email the database');
                                        }
                                    });
                                }, 2000);
                            }
                        });
                    }, 5000);
                });
            }, 2000);
        });
    }


    // set side panel size
    setSidePanelSize() {
        if ($(window).width() <= 720) {
            $('body').attr('panel-open', false);
        } else {
            $('body').attr('panel-open', true);
        }
    }
}

var global = new globalFunctionalities();
