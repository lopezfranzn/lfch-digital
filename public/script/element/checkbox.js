// select month
$(document).on('click', '.checklist > .item', function(event) {
    event.preventDefault();

    if ($(this).find('[data-icon]').hasClass('fa-check-square')) {
        $(this).attr('check', 'false');
        $(this).next('.item-below').addClass('hidden');
        $(this).find('[data-icon]').remove();
        $(this).prepend('<i class="far fa-square"></i>');
    } else {
        $(this).attr('check', 'true');
        $(this).next('.item-below').removeClass('hidden');
        $(this).find('[data-icon]').remove();
        $(this).prepend('<i class="far fa-check-square"></i>');
    }
});
