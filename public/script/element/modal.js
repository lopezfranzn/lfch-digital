// close modal
$(document).on('click', '.close-modal, a[data-close="modal"]', function(event) {
    if ($(this).attr('href').length == 0 || $(this).attr('href') == '#') {
        event.preventDefault();
    }

    $('.modal-container').removeClass('active');

    setTimeout(function(){
        $('.modal-container .title, .modal-container .content, .modal-container .action').html('');
    }, 100);
});
