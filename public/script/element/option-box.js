$(document).on('click', '.selection-list', function(event) {
    event.stopPropagation();
    global.closeAllClosables(this);
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('.list').delay(300).fadeOut('fast');
    } else {
        $(this).find('.list').show();
        $(this).addClass('active');
    }

    if ($(this).find('.carret').attr('src') == null) {
        $(this).find('.carret').attr('src', global.root + 'public/system/images/selection-list-carret-light.png');
    }
});

$(document).on('click', '.selection-list .list span', function(event) {
    event.stopPropagation();
    if ($(this).parents('.selection-list').hasClass('data-type')) {
        $(this).parents('.selection-list').find('span > span').attr('data-value', $(this).attr('data-value')).html($(this).html()).css('font-family', $(this).css('font-family'));
        $(this).parents('.selection-list').addClass('changed');
    } else {
        global.closeAllClosables();
        $(this).parents('.selection-list').addClass('active');
        $(this).parents('.selection-list').find('.list').show();
    }
});
