// select month
$(document).on('click', '.date-month .overflow > span', function() {
    $(this).parents('.date-container').find('.date-day').removeClass('changed');
    $(this).parents('.date-container').find('.date-day .overflow > span').show();
    $(this).parents('.date-container').find('.date-day .main-value').removeAttr('data-value').text('Select Day');

    if ($(this).attr('less') == '1') {
        $(this).parents('.date-container').find('.date-day .overflow > span[data-value="31"]').hide();
    } else if ($(this).attr('less') == '2') {
        $(this).parents('.date-container').find('.date-day .overflow > span[data-value="31"]').hide();
        $(this).parents('.date-container').find('.date-day .overflow > span[data-value="30"]').hide();
    }
});

// clear date picker
$(document).on('click', '.date-container .clear-field', function(event) {
    event.preventDefault();
    var that = $(this).parents('.date-container');
    that.find('.selection-list').removeClass('changed');
    that.find('.main-value').removeAttr('data-value');
    that.find('.date-month').find('.main-value').text('Select Month');
    that.find('.date-day').find('.main-value').text('Select Day');
    that.find('.date-year').find('.main-value').text('Select Year');
});
