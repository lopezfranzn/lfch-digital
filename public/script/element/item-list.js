// add item in the list
$(document).on('click', '.item-list .add-item', function(event) {
    event.preventDefault();
    var parent = $(this).parent('.item-list');
    var placeholder = parent.attr('placeholders');
    $(this).remove();
    parent.append('<div class="item"><input type="text" placeholder="' + placeholder + '" /><i class="fa fa-times"></i></div>'
        + '<a href="#" class="add-item"><i class="fa fa-plus"></i>Add item</a>');
});

// remove item from the list
$(document).on('click', '.item-list .item .fa-times', function(event) {
    event.preventDefault();
    var total = $(this).parents('.item-list').find('.item').length;

    if (total != 1) {
        var parent = $(this).parent('.item');
        parent.remove();
    }
});
