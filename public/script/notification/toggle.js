// open notification
$(document).on('click', 'a[title="Notifications"]', function(event) {
    event.preventDefault();

    $('.notification-container').addClass('active');

    if ($('a[title="Notifications"] .counter').text() != '') {
        // clear all notifications
        global.clearNotifications();
    }
});

// close notification
$(document).on('click', '[data-close="notification"]', function(event) {
    event.preventDefault();

    $('.notification-container').removeClass('active');

    if ($('a[title="Notifications"] .counter').text() != '') {
        // clear all notifications
        global.clearNotifications();
    }
});
