// pull notifications
setInterval(function(){
    if (!$('body').hasClass('login-page')) {
        if (!$('body').hasClass('notification-fetching')) {
            $('body').addClass('notification-fetching');

        	// get notification total
            var atype = $('.user-info').attr('atype');

            $.get(global.root + 'includes/query/notification/total.php?target=' + atype, function(data){
                // get result count from storage
                var pulledTotal = localStorage.getItem('pulledNotif');

                // if has an update
                if (data != pulledTotal) {
                    // fetch notifications
                    global.reloadNotification();

                    // set result count in storage
                    localStorage.setItem('pulledNotif', data);

                    // reload patient list
                    global.reloadPatientList();

                	// get unread notification total
                    global.getUnreadNotifCount();

                    // play sound
                    if ($('#notification-audio').length > 0) {
                        $('#notification-audio').get(0).play();
                    }
                }

                setTimeout(()=> {
                    $('body').removeClass('notification-fetching');
                }, (Math.floor(Math.random() * 10) * 5000));
            });
        }
    }
}, (Math.floor(Math.random() * 10) * 3000));
