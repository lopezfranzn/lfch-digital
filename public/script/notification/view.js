// view notification
$(document).on('click', '.view-notification', function(event) {
    event.preventDefault();

    if ($(this).attr('patient-id') != undefined) {
        var targetPatient = $(this).attr('patient-id');
        localStorage.setItem('pID', targetPatient);
        $('.panel-item[pin="patient-profile"]').click();
    }
});
