// get result count and set to storage
if (!$('body').hasClass('login-page')) {
    var uID = $('.user-info').attr('uid');
    $.get(global.root + 'includes/query/message/total.php?mto=' + uID, function(data){
        localStorage.setItem('pulledMessages', data);
    });
}

global.getUnreadMessageCount();

// pull notifications
setInterval(function(){
    if (!$('body').hasClass('login-page')) {
        if (!$('body').hasClass('message-fetching')) {
            $('body').addClass('message-fetching');

        	// get notification total
            var uID = $('.user-info').attr('uid');

            $.get(global.root + 'includes/query/message/total.php?mto=' + uID, function(data){
                // get result count from storage
                var pulledTotal = localStorage.getItem('pulledMessages');

                // if has an update
                if (data != pulledTotal) {
                    // fetch messages
                    global.reloadMessages();

                    // set result count in storage
                    localStorage.setItem('pulledMessages', data);

                	// get unread messages total
                    global.getUnreadMessageCount();

                    // play sound
                    if ($('#message-audio').length > 0) {
                        $('#message-audio').get(0).play();
                    }
                }

                setTimeout(()=> {
                    $('body').removeClass('message-fetching');
                }, (Math.floor(Math.random() * 10) * 5000));
            });
        }
    }
}, (Math.floor(Math.random() * 10) * 5000));
