// send message
$(document).on('click', '.send-message', function(event) {
    event.preventDefault();

    var mto = $('.person.active').attr('uid');
    var mfrom = $('.user-info').attr('uid');
    var content = $('input.message-content').val();

    if (content.length == 0) {
        $.get(global.root + 'public/system/data/modal.json', function(data){
            var md = data['inputBlank']
            global.openModal(md.title, md.content, md.button, md.size);
            $('input.message-content').val('');
        });
    } else {
        if ($('.person').hasClass('active')) {
            if (!(content.length > 300)) {
                $.get(global.root + 'includes/query/message/add.php?mfrom=' + mfrom + '&mto=' + mto + '&content=' + content, function(data){
                    if (data == 'true') {
                        $('.person.active').click();
                        $('input.message-content').val('');
                    } else {
                        console.log(data);

        		        $.get(global.root + 'public/system/data/modal.json', function(data){
        					var md = data['databaseFailed']
        		            global.openModal(md.title, md.content, md.button, md.size);
        		        });
                    }
                });
            } else {
                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['maxMessage']
                    global.openModal(md.title, md.content, md.button, md.size);
                });
            }
        } else {
            $.get(global.root + 'public/system/data/modal.json', function(data){
                var md = data['noContact']
                global.openModal(md.title, md.content, md.button, md.size);
            });
        }
    }
}).keypress(function(event) {
    if(event.which == 13) {
        if ($('body').hasClass('messages-page')) {
            $('.send-message').click();
        }
    }
});


// select contact
$(document).on('click', '.new-conversation', function(event) {
    event.preventDefault();

    $.get(global.root + 'public/system/data/modal.json', function(data){
        var md = data['contactList']
        global.openModal(md.title, md.content, md.button, md.size);
        $('input.message-content').val('');

        $.get(global.root + 'includes/query/user/view.php', function(data){
            if (data != null) {
                for (var i = 0; i < data.length; i++) {
                    var currentCount = i + 1;
                    var uID = $('.user-info').attr('uid');

                    if (data[i].id != uID) {
                        $('.modal .contact-list').append('<a href="#" class="contact-item" uid="' + data[i].id + '" data-close="modal"></a>');
                        $('.contact-item[uid="' + data[i].id + '"]').append('<span class="image" style="background-image: url(' + global.root + 'public/system/images/avatar/' + data[i].image + '.jpg)"></span><span class="name"></span><i class="far fa-angle-right"></i><div class="clear"></div>');

                        if (data[i].atype == 'admin') {
                            $('.contact-item[uid="' + data[i].id + '"] .name').text('Dr. ' + data[i].fname + ' ' + data[i].lname);
                        } else {
                            $('.contact-item[uid="' + data[i].id + '"] .name').text(data[i].fname + ' ' + data[i].lname);
                        }
                    }
                }
            }
        });
    });
});

// select contact
$(document).on('click', '.modal .contact-item', function(event) {
    event.preventDefault();

    var uID = $(this).attr('uid');
    var imgSrc = $(this).find('.image').attr('style');
    var name = $(this).find('.name').text();

    if ($('.people-list .list .person[uid="' + uID + '"]').attr('uid') == uID) {
        $('.people-list .list .person[uid="' + uID + '"]').click();
    } else {
        $('.people-list .list').append('<a href="#" class="person" uid="' + uID + '"><span class="image" style="' + imgSrc + '"></span><span class="name">'+ name + '</span><span class="excerpt">No message</span><i class="far fa-angle-right"></i><div class="clear"></div></a>');

        $('.people-list .list .person[uid="' + uID + '"]').click();
    }
});
