$(document).on('click', '.people-list .list .person', function(event) {
    event.preventDefault();
    var targetConvo = $(this).attr('uid');
    var uID = $('.user-info').attr('uid');
    var that = $(this);

    $('.messages-container').html('');
    localStorage.setItem('convoID', targetConvo);
    $('.people-list .list .person').removeClass('active');
    that.addClass('active');

    $.get(global.root + '/includes/query/message/conversation.php?mfrom=' + uID + '&mto=' + targetConvo, function(data){
        if (data != 0) {
            global.clearMessages(targetConvo);

            for (var i = 0; i < data.length; i++) {
                var currentCount = i + 1;
                var messageID = 'msg-' + data[i].id;
                $('.messages-container').prepend('<div class="' + i + ' new" id="' + messageID + '"></div>').prepend('<div class="clear"></div>');
                $('div.' + i + '.new').prepend('<span class="date">' + data[i].trn_date + '</span>');
                $('div.' + i + '.new').prepend('<span class="content">' + data[i].content + '</span>');

                if (data[i].mfrom == uID) {
                    $('.messages-container .' + i + '.new').attr('sender', 'me');
                } else {
                    $('.messages-container .' + i + '.new').attr('sender', 'other');
                }

                if (currentCount == data.length) {
                    $('.messages-container > div:not(".clear")').removeAttr('class').addClass('message-item');

                    var targetMsg = $('.messages-container > .message-item:last-child').attr('id');
                    $('.messages-container').animate({
                        scrollTop: $("#" + targetMsg).offset().top
                    }, 0);
                }
            }
        }
    });
});
