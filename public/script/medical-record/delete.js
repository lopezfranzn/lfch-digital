$(document).on('click', '.rec-delete', function(event) {
    event.preventDefault();
    var that = $(this);
    var dataTarget = $(this).attr('data-delete');
    var recID = $(this).attr('rec-id');

    $.get(global.root + 'public/system/data/modal.json', function(data){
        var md = data['deleteMedRecord']
        global.openModal(md.title, md.content, md.button, md.size);

        $('.modal .confirm-rec-delete').attr({
            'rec-target-id': recID,
            'rec-target-type': dataTarget
        });

        $('.modal .content > span i').html(that.parents('.item').find('.item-title').text());
    });
});

$(document).on('click', '.confirm-rec-delete', function(event) {
    event.preventDefault();
    var dataTarget = $(this).attr('rec-target-type');
    var recID = $(this).attr('rec-target-id');

    $.get(global.root + '/includes/query/medical-record/delete.php?id=' + recID, function(response){
        if (response == 'true') {
            if (dataTarget == 'pe') {
                $('.physical-exam [rec-id="' + recID + '"]').parents('.item').remove();

			    // masonry
			    $('.physical-exam .values').masonry('destroy').masonry({
			        itemSelector: '.item',
			        columnWidth: 210,
			        isFitWidth: true
			    });

                setTimeout(function(){
                    var recLength = $('.physical-exam .values > .item').length;
                    if (recLength == 0) {
    					$('.physical-exam .values').masonry('destroy').html(global.noData + '<div class="clear"></div>');
                        $('.physical-exam .load-more-result').remove();
                    } else {
                        if ($('.physical-exam .load-more-result:contains("more")').length == 0) {
                            var recCurrentTotal = $('.physical-exam .load-more-result').text().replace(/\D/g,'');
                            var recTotal = parseInt(recCurrentTotal) - 1;
                            $('.physical-exam .load-more-result').html('All (' + recTotal + ') result/s loaded');
                        } else {
                            $('.physical-exam .load-more-result').click();
                        }
                    }
                }, 100);
            } else if (dataTarget == 'lr') {
                $('.lab-result [rec-id="' + recID + '"]').parents('.item').remove();

			    // masonry
			    $('.lab-result .values').masonry('destroy').masonry({
			        itemSelector: '.lab-result .values > .item',
			        columnWidth: 210,
			        isFitWidth: true
			    });

                setTimeout(function(){
                    var recLength = $('.lab-result .values > .item').length;
                    if (recLength == 0) {
    					$('.lab-result .values').masonry('destroy').html(global.noData + '<div class="clear"></div>');
                        $('.lab-result .load-more-result').remove();
                    } else {
                        if ($('.lab-result .load-more-result:contains("more")').length == 0) {
                            var recCurrentTotal = $('.lab-result .load-more-result').text().replace(/\D/g,'');
                            var recTotal = parseInt(recCurrentTotal) - 1;
                            $('.lab-result .load-more-result').html('All (' + recTotal + ') result/s loaded');
                        } else {
                            $('.lab-result .load-more-result').click();
                        }
                    }
                }, 100);
            } else if (dataTarget == 'at') {
                $('.assessment [rec-id="' + recID + '"]').parents('.item').remove();

			    // masonry
			    $('.assessment .values').masonry('destroy').masonry({
			        itemSelector: '.assessment .values > .item',
			        columnWidth: 210,
			        isFitWidth: true
			    });

                setTimeout(function(){
                    var recLength = $('.assessment .values > .item').length;
                    if (recLength == 0) {
    					$('.assessment .values').masonry('destroy').html(global.noData + '<div class="clear"></div>');
                        $('.assessment .load-more-result').remove();
                    } else {
                        if ($('.assessment .load-more-result:contains("more")').length == 0) {
                            var recCurrentTotal = $('.assessment .load-more-result').text().replace(/\D/g,'');
                            var recTotal = parseInt(recCurrentTotal) - 1;
                            $('.assessment .load-more-result').html('All (' + recTotal + ') result/s loaded');
                        } else {
                            $('.assessment .load-more-result').click();
                        }
                    }
                }, 100);
            }
        } else {
            console.log(data);

            $.get(global.root + 'public/system/data/modal.json', function(data){
                var md = data['databaseFailed']
                global.openModal(md.title, md.content, md.button, md.size);
            });
        }
    });
});
