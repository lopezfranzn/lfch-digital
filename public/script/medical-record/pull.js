$(document).on('click', '.load-more-result', function(event) {
    event.preventDefault();
    var dataTarget = $(this).attr('data-target');
    var pID = localStorage.getItem('pID');

    if (!$(this).hasClass('completed')) {
        if (dataTarget == 'pe') {
			$.get(global.root + '/includes/query/medical-record/pull.php?pid=' + pID + '&rtype=pe&limit=' + global.medRecResultLimit + '&offset=' + global.peTotal, function(record) {
				record = JSON.parse(record);

				if (record.length != 0) {
                    $('.physical-exam .values .clear').remove();

					for (var i = 0; i < record.length; i++) {
						var currentCount = i + 1;
						var newNumbers = global.peTotal + currentCount;
			            var physicalExam = JSON.parse(record[i].content);
			            var stamp = record[i].trn_date;

						$('.physical-exam .values').append('<div class="item">'
						+ '<span class="item-title">Physical Exam #' + newNumbers + '</span>'
						+ '<span class="pe-bp" display="' + global.checkIfNull(physicalExam[0].bp) + '">BP<i class="sub">' + physicalExam[0].bp + '</i></span>'
						+ '<span class="pe-cr" display="' + global.checkIfNull(physicalExam[0].cr) + '">CR<i class="sub">' + physicalExam[0].cr + '</i></span>'
						+ '<span class="pe-temp" display="' + global.checkIfNull(physicalExam[0].temp) + '">Temp<i class="sub">' + physicalExam[0].temp + '</i></span>'
						+ '<span class="pe-abdomen-fh" display="' + global.checkIfNull(physicalExam[0].abdomen.fh) + '">Abdomen FH<i class="sub">' + physicalExam[0].abdomen.fh + '</i></span>'
						+ '<span class="pe-abdomen-fht" display="' + global.checkIfNull(physicalExam[0].abdomen.fht) + '">Abdomen Fht<i class="sub">' + physicalExam[0].abdomen.fht + '</i></span>'
						+ '<span class="pe-ie" display="' + global.checkIfNull(physicalExam[0].ie) + '">IE<i class="sub">' + physicalExam[0].ie + '</i></span>'
						+ '<span class="pe-utz" display="' + global.checkIfNull(physicalExam[0].utz) + '">UTZ<i class="sub">' + physicalExam[0].utz + '</i></span>'
						+ '<span class="pe-others" display="' + global.checkIfNull(physicalExam[0].others) + '">Others<i class="sub">' + physicalExam[0].others + '</i></span>'
						+ '<span class="pe-stamp stamp" display="true"><i class="sub">' + global.detailedDate(stamp) + '</i></span>'
						+ '<a href="#" class="rec-delete" data-delete="pe" rec-id="' + record[i].id + '">✖</a>'
						+ '<div class="clear"></div></div>');

						if (currentCount == record.length) {
							$('.physical-exam .values').append('<div class="clear"></div>');

						    // masonry
						    $('.physical-exam .values').masonry('destroy').masonry({
						        itemSelector: '.item',
						        columnWidth: 210,
						        isFitWidth: true
						    });

							global.peTotal = global.peTotal + record.length;

							$.get(global.root + '/includes/query/medical-record/total.php?pid=' + pID + '&rtype=pe', function(total) {
								if (global.peTotal < total) {
									$('.physical-exam .load-more-result').html('<i class="far fa-sync"></i>Load more');
								} else {
									$('.physical-exam .load-more-result').addClass('completed').html('All (' + total + ') result/s loaded');
								}
							});
						}
					}
				}
			});
        } else if (dataTarget == 'lr') {
			$.get(global.root + '/includes/query/medical-record/pull.php?pid=' + pID + '&rtype=lr&limit=' + global.medRecResultLimit + '&offset=' + lrTotal, function(record){
				record = JSON.parse(record);

				if (record.length != 0) {
                    $('.lab-result .values .clear').remove();

					for (var i = 0; i < record.length; i++) {
						var currentCount = i + 1;
						var newNumbers = lrTotal + currentCount;
			            var labResult = JSON.parse(record[i].content);
			            var stamp = record[i].trn_date;

						$('.lab-result .values').append('<div class="item">'
							+ '<span class="item-title">Lab Result #' + newNumbers + '</span>'
							+ '<ul class="sub lab-result-list-' + i + '"></ul>'
							+ '<span class="lab-result-stamp stamp" display="true"><i class="sub">' + global.detailedDate(stamp) + '</i></span>'
							+ '<a href="#" class="rec-delete" data-delete="lr" rec-id="' + record[i].id + '">✖</a>'
							+ '<div class="clear"></div></div>');
						global.fetchList('lab-result-list-' + i, labResult);

						if (currentCount == record.length) {
							$('.lab-result .values').append('<div class="clear"></div>');

						    // masonry
						    $('.lab-result .values').masonry('destroy').masonry({
						        itemSelector: '.lab-result .values > .item',
						        columnWidth: 210,
						        isFitWidth: true
						    });

							lrTotal = lrTotal + record.length;

							$.get(global.root + '/includes/query/medical-record/total.php?pid=' + pID + '&rtype=lr', function(total) {
								if (lrTotal < total) {
									$('.lab-result .load-more-result').html('<i class="far fa-sync"></i>Load more');
								} else {
									$('.lab-result .load-more-result').addClass('completed').html('All (' + total + ') result/s loaded');
								}
							});
						}
					}
				}
			});
        } else if (dataTarget == 'at') {
			$.get(global.root + '/includes/query/medical-record/pull.php?pid=' + pID + '&rtype=at&limit=' + global.medRecResultLimit + '&offset=' + atTotal, function(record){
				record = JSON.parse(record);

				if (record.length != 0) {
                    $('.assessment .values .clear').remove();

					for (var i = 0; i < record.length; i++) {
						var currentCount = i + 1;
						var newNumbers = atTotal + currentCount;
			            var assessment = JSON.parse(record[i].content);
			            var stamp = record[i].trn_date;

						$('.assessment .values').append('<div class="item">'
							+ '<span class="item-title">Assessment #' + newNumbers + '</span>'
							+ '<ul class="sub assessment-list-' + i + '"></ul>'
							+ '<span class="assessment-stamp stamp" display="true"><i class="sub">' + global.detailedDate(stamp) + '</i></span>'
							+ '<a href="#" class="rec-delete" data-delete="at" rec-id="' + record[i].id + '">✖</a>'
							+ '<div class="clear"></div></div>');
						global.fetchList('assessment-list-' + i, assessment);

						if (currentCount == record.length) {
							$('.assessment .values').append('<div class="clear"></div>');

						    // masonry
						    $('.assessment .values').masonry('destroy').masonry({
						        itemSelector: '.assessment .values > .item',
						        columnWidth: 210,
						        isFitWidth: true
						    });

							atTotal = atTotal + record.length;

							$.get(global.root + '/includes/query/medical-record/total.php?pid=' + pID + '&rtype=at', function(total) {
								if (atTotal < total) {
									$('.assessment .load-more-result').html('<i class="far fa-sync"></i>Load more');
								} else {
									$('.assessment .load-more-result').addClass('completed').html('All (' + total + ') result/s loaded');
								}
							});
						}
					}
				}
			});
        }
    }
});
