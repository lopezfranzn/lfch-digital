// popup warning
$(document).on('click', '.functions .delete-patient', function(event) {
    event.preventDefault();
    var targetID = $(this).attr('target-patient');

    $.get(global.root + 'public/system/data/modal.json', function(data){
        var md = data['deletePatient']
        global.openModal(md.title, md.content, md.button, md.size);
        $('.confirm-patient-delete').attr('target-patient', targetID);
    });
});

// confirm deletion
$(document).on('click', '.confirm-patient-delete', function(event) {
    event.preventDefault();
    var targetID = localStorage.getItem('pID');
    var fname = $('.fname').text();
    var mname = $('.mname').text();
    var lname = $('.lname').text();

    setTimeout(function(){
        $.get(global.root + 'includes/query/patient/delete.php?id=' + targetID, function(data){
            if (data == 'true') {
                // send to email
                var sender = $('.user-info .name').text();
                var msubject = 'Patient Record Removed';
                var mcontent = sender + ' removed ' + fname + ' ' + mname + ' ' + lname + ' from the record.';
                global.pushEmail(msubject, mcontent);

                // push notification
                var nfrom = $('.user-info').attr('uid');
                var target;
                var content = 'removed <i>' + fname + ' ' + mname + ' ' + lname + '</i> from the record';

                if ($('.user-info').attr('atype') == 'admin') {
                    target = 'standard';
                } else {
                    target = 'admin';
                }

                global.pushNotification(nfrom, target, content);

                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['deletePatientSuccess']
                    global.openModal(md.title, md.content, md.button, md.size);
                });

                $('.panel-item[pin="patients"]').click();
            } else {
                console.log(data);

		        $.get(global.root + 'public/system/data/modal.json', function(data){
					var md = data['databaseFailed']
		            global.openModal(md.title, md.content, md.button, md.size);
		        });
            }
        });
    }, 100);
});
