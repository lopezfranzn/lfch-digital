// edit patient
$(document).on('click', '.edit-patient', function(event) {
    event.preventDefault();

    $('.panel-item[pin="edit-patient"]').click();
});

// save edits
$(document).on('submit', '#edit-patient', function(event) {
    event.preventDefault();

    // total field error
    var fieldError;
    fieldError = 0;

    // remove empty nested item on the lists
    var itemListNestedCount = $('.item-list input[type="text"]').length;

    $('.item-list input[type="text"]').each(function(i) {
        var currentCount = i + 1;

        if ($(this).val().length == 0) {
            $(this).parent('.item').remove();
        }

        if (currentCount == itemListNestedCount) {
            // auto-generated data
            var date = $('#edit-patient').attr('date');
            var image = $('#edit-patient').attr('image-number');

            // full name
            var lname = $('input[name="lname"]').val();
            var fname = $('input[name="fname"]').val();
            var mname = $('input[name="mname"]').val();

            if (lname.length == 0 ||
                fname.length == 0 ||
                mname.length == 0) {
                fieldError = fieldError + 1;
                $('#add-patient').attr('field-error', fieldError);

                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['fieldIsRequired'];
                    global.openModal(md.title, md.content, md.button, md.size);
                    $('.modal .required-fields').text('Full Name');
                });
            } else {
                if (parseInt($('#add-patient').attr('field-error')) < 0) {
                    fieldError = fieldError - 1;
                    $('#add-patient').attr('field-error', fieldError);
                }
            }

            // birth date
            var birthDate = global.getDateValue('birth-date');

            if (birthDate == null) {
                fieldError = fieldError + 1;
                $('#add-patient').attr('field-error', fieldError);

                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['fieldIsRequired'];
                    global.openModal(md.title, md.content, md.button, md.size);
                    $('.modal .required-fields').text('Birth Date');
                });
            } else {
                if (parseInt($('#add-patient').attr('field-error')) < 0) {
                    fieldError = fieldError - 1;
                    $('#add-patient').attr('field-error', fieldError);
                }
            }

            // contact number
            var contactNo = $('input[name="contactno"]').val();

            if (contactNo.length == 0) {
                fieldError = fieldError + 1;
                $('#add-patient').attr('field-error', fieldError);

                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['fieldIsRequired'];
                    global.openModal(md.title, md.content, md.button, md.size);
                    $('.modal .required-fields').text('Contact Number');
                });
            } else {
                if ($.isNumeric(contactNo)) {
                    if (parseInt($('#add-patient').attr('field-error')) < 0) {
                        fieldError = fieldError - 1;
                        $('#add-patient').attr('field-error', fieldError);
                    }
                } else {
                    fieldError = fieldError + 1;
                    $('#add-patient').attr('field-error', fieldError);

                    $.get(global.root + 'public/system/data/modal.json', function(data){
                        var md = data['contactNAN'];
                        global.openModal(md.title, md.content, md.button, md.size);
                    });
                }
            }

            // address
            var address = $('input[name="address"]').val();

            if (address.length == 0) {
                fieldError = fieldError + 1;
                $('#add-patient').attr('field-error', fieldError);

                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['fieldIsRequired'];
                    global.openModal(md.title, md.content, md.button, md.size);
                    $('.modal .required-fields').text('Address');
                });
            } else {
                if (parseInt($('#add-patient').attr('field-error')) < 0) {
                    fieldError = fieldError - 1;
                    $('#add-patient').attr('field-error', fieldError);
                }
            }

            // past medical history
            var hypertension = $('a[data-value="hypertension"]').attr('check');
            var diabetesMellitus = $('a[data-value="diabetes-mellitus"]').attr('check');
            var bronchialAsthma = $('a[data-value="bronchial-asthma"]').attr('check');
            var bronchialAsthmaDate = global.getDateValue('bronchial-asthma-date');
            var surgery = $('a[data-value="surgery"]').attr('check');
            var pastmedhistory;

            if (hypertension == 'false' &&
                diabetesMellitus == 'false' &&
                bronchialAsthma == 'false' &&
                bronchialAsthmaDate == null &&
                surgery == 'false' &&
                global.getListValue('surgery-list') == undefined) {
                pastmedhistory = null;
            } else {
                if (global.getListValue('surgery-list') == undefined) {
                    pastmedhistory = '[{"hypertension":"' + hypertension + '","diabetesMellitus":"' + diabetesMellitus + '","bronchialAsthma":{"value":"' + bronchialAsthma + '","date":"' + bronchialAsthmaDate + '"},"surgery":{"value":"' + surgery + '","list":"null"}}]';
                } else {
                    pastmedhistory = '[{"hypertension":"' + hypertension + '","diabetesMellitus":"' + diabetesMellitus + '","bronchialAsthma":{"value":"' + bronchialAsthma + '","date":"' + bronchialAsthmaDate + '"},"surgery":{"value":"' + surgery + '","list":[' + global.getListValue('surgery-list') + ']}}]';
                }
            }

            // last menstrual history
            var lastMenstrualDate = global.getDateValue('last-menstrual-date');

            // age of gestation
            var ageGestation;
            if ($('input[name="age-gestation"]').val().length == 0) {
                ageGestation = null;
            } else {
                ageGestation = $('input[name="age-gestation"]').val();
            }

            // ob history
            var gravida = $('.ob-history-gravida .main-value').attr('data-value');
            var para = $('.ob-history-para .main-value').attr('data-value');
            var obHistoryS1 = $('.ob-history-spaces div[field-count="1"] .main-value').attr('data-value');
            var obHistoryS2 = $('.ob-history-spaces div[field-count="2"] .main-value').attr('data-value');
            var obHistoryS3 = $('.ob-history-spaces div[field-count="3"] .main-value').attr('data-value');
            var obHistoryS4 = $('.ob-history-spaces div[field-count="4"] .main-value').attr('data-value');
            var obHistorySpaces = obHistoryS1 + '-'
                + obHistoryS2 + '-'
                + obHistoryS3 + '-'
                + obHistoryS4;
            var obHistory;

            if (gravida == undefined &&
                para == undefined &&
                obHistoryS1 == undefined &&
                obHistoryS2 == undefined &&
                obHistoryS3 == undefined &&
                obHistoryS4 == undefined) {
                if (parseInt($('#add-patient').attr('field-error')) < 0) {
                    fieldError = fieldError - 1;
                    $('#add-patient').attr('field-error', fieldError);
                }

                obHistory = null;
            } else if (gravida == undefined ||
                para == undefined ||
                obHistoryS1 == undefined ||
                obHistoryS2 == undefined ||
                obHistoryS3 == undefined ||
                obHistoryS4 == undefined) {
                fieldError = fieldError + 1;
                $('#add-patient').attr('field-error', fieldError);

                $.get(global.root + 'public/system/data/modal.json', function(data){
                    var md = data['obHistoryInc']
                    global.openModal(md.title, md.content, md.button, md.size);
                });
            } else {
                if (parseInt($('#add-patient').attr('field-error')) < 0) {
                    fieldError = fieldError - 1;
                    $('#add-patient').attr('field-error', fieldError);
                }

                obHistory = '[{"gravida":"' + gravida + '","para":"' + para + '","digits":"' + obHistorySpaces + '"}]';
            }

            // chief complaint
            var chiefComplaint;
            if (global.getListValue('chief-complaint-list') == undefined) {
                chiefComplaint = null;
            } else {
                chiefComplaint = '[{"list":[' + global.getListValue('chief-complaint-list') + ']}]';
            }

            // plan
            var plan;
            if (global.getListValue('plan-list') == undefined) {
                plan = null;
            } else {
                plan = '[{"list":[' + global.getListValue('plan-list') + ']}]';
            }

            // follow up
            var followUp;
            var followUpDate = global.getDateValue('followup-date');

            if (followUpDate == null) {
                followUp = null;
            } else {
                if (global.getListValue('followup-instruction-list') == undefined) {
                    followUp = '[{"date":"' + followUpDate + '","list":"null"}]';
                } else {
                    followUp = '[{"date":"' + followUpDate + '","list":[' + global.getListValue('followup-instruction-list') + ']}]';
                }
            }

            // if has no error on form then save
            setTimeout(function() {
                if (!(fieldError > 0)) {
                    // save to database
                    $.get(global.root + 'includes/query/patient/edit.php?'
                        // auto-generated data
                        + 'id=' + localStorage.getItem('pID')
                        // auto-generated data
                        + '&image=' + image
                        // full name
                        + '&lname=' + lname
                        + '&fname=' + fname
                        + '&mname=' + mname
                        // birth date
                        + '&birth_date=' + birthDate
                        // contact information
                        + '&contact_no=' + contactNo
                        + '&address=' + address
                        // past medical history
                        + '&past_med_history=' + pastmedhistory
                        // last menstrual history
                        + '&last_mens_history=' + lastMenstrualDate
                        // age of gestation
                        + '&age_gestation=' + ageGestation
                        // ob history
                        + '&ob_history=' + obHistory
                        // chief complaint
                        + '&chief_complaint=' + chiefComplaint
                        // // physical exam
                        // + '&physical_exam=' + newPhysicalExam
                        // // lab results
                        // + '&lab_result=' + newLabResult
                        // // assessment
                        // + '&assessment=' + newAssessment
                        // plan
                        + '&plan=' + plan
                        // follow up
                        + '&follow_up=' + followUp
                        // date today
                        + '&trn_date=' + date, function(data){

                        if (data == 'true') {
                            // physical exam
                            var bP = $('input[name="pe-bp"]').val();
                            var cR = $('input[name="pe-cr"]').val();
                            var temp = $('input[name="pe-temp"]').val();
                            var fH = $('input[name="pe-abdomen-fh"]').val();
                            var fHT = $('input[name="pe-abdomen-fht"]').val();
                            var iE = $('input[name="pe-ie"]').val();
                            var uTZ = $('input[name="pe-utz"]').val();
                            var pEOthers = $('input[name="pe-others"]').val();
                            var physicalExam;

                            if (bP.length == 0 &&
                                cR.length == 0 &&
                                temp.length == 0 &&
                                fH.length == 0 &&
                                fHT.length == 0 &&
                                iE.length == 0 &&
                                uTZ.length == 0 &&
                                pEOthers.length == 0) {
                                // physicalExam = null;
                            } else {
                                physicalExam = '[{"bp":"' + bP.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '","cr":"' + cR.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '","temp":"' + temp.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '","abdomen":{"fh":"' + fH.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '","fht":"' + fHT.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '"},"ie":"' + iE.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '","utz":"' + uTZ.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '","others":"' + pEOthers.replace(/"/g, 'SYSDQUOTE').replace(/'/g, 'SYSQUOTE') + '"}]';

                                $.get(global.root + '/includes/query/medical-record/add.php?pid=' + localStorage.getItem('pID') + '&rtype=pe&content=' + physicalExam + '&stamp=' + global.getDate('-', 'ymd'), function(response){
                                    if (response != 'true') {
                                        console.log(response);

                                        $.get(global.root + 'public/system/data/modal.json', function(data){
                                            var md = data['databaseFailed'];
                                            global.openModal(md.title, md.content, md.button, md.size);
                                        });
                                    }
                                });
                            }

                            // lab results
                            var labResultList = global.getListValue('lab-result-list');
                            var labResult;

                            if (labResultList != undefined) {
                                labResult = '[' + labResultList + ']';

                                $.get(global.root + '/includes/query/medical-record/add.php?pid=' + localStorage.getItem('pID') + '&rtype=lr&content=' + labResult + '&stamp=' + global.getDate('-', 'ymd'), function(response){
                                    if (response != 'true') {
                                        console.log(response);

                                        $.get(global.root + 'public/system/data/modal.json', function(data){
                                            var md = data['databaseFailed'];
                                            global.openModal(md.title, md.content, md.button, md.size);
                                        });
                                    }
                                });
                            } else {
                                // labResult = null;
                            }

                            // assessment
                            var assessmentList = global.getListValue('assessment-list');
                            var assessment;

                            if (assessmentList != undefined) {
                                assessment = '[' + assessmentList + ']';

                    			$.get(global.root + '/includes/query/medical-record/add.php?pid=' + localStorage.getItem('pID') + '&rtype=at&content=' + assessment + '&stamp=' + global.getDate('-', 'ymd'), function(response){
                                    if (response != 'true') {
                                        console.log(response);

                                        $.get(global.root + 'public/system/data/modal.json', function(data){
                                            var md = data['databaseFailed'];
                                            global.openModal(md.title, md.content, md.button, md.size);
                                        });
                                    }
                                });
                            } else {
                                // assessment = null;
                            }

                            // send to email
                            var sender = $('.user-info .name').text();
                            var msubject = 'Patient Edited';
                            var mcontent = sender + ' edited the record of ' + fname + ' ' + mname + ' ' + lname + '.';
                            global.pushEmail(msubject, mcontent);

                            // push notification
                            var pID = localStorage.getItem('pID');
                            var nfrom = $('.user-info').attr('uid');
                            var target;
                            var content = 'edited the record of <i target-patient="' + pID + '">' + fname + ' ' + mname + ' ' + lname + '</i>';

                            if ($('.user-info').attr('atype') == 'admin') {
                                target = 'standard';
                            } else {
                                target = 'admin';
                            }

                            global.pushNotification(nfrom, target, content);

                            // go to patient profile
                            setTimeout(function(){
                                $('.panel-item[pin="patient-profile"]').click();
                            }, 1000);

                            $.get(global.root + 'public/system/data/modal.json', function(data){
                                var md = data['updatePatient']
                                global.openModal(md.title, md.content, md.button, md.size);
                            });
                        } else {
                            console.log(data);

                            $.get(global.root + 'public/system/data/modal.json', function(data){
                                var md = data['databaseFailed']
                                global.openModal(md.title, md.content, md.button, md.size);
                            });
                        }
                    });
                }
            }, 100);
        }
    });
});

// edit follow up status
$(document).on('click', '.complete-follow-up', function(event) {
    event.preventDefault();
    var pID = localStorage.getItem('pID');
    var status = 'true';

    $.get(global.root + 'includes/query/schedule/toggle.php?id=' + pID + '&status=' + status, function(data){
        if (data != 'true') {
            console.log(data);

            $.get(global.root + 'public/system/data/modal.json', function(result){
                var md = result['databaseFailed']
                global.openModal(md.title, md.content, md.button, md.size);
            });
        } else {
            $('.patient-info .followup-date > .link')
                .removeClass('complete-follow-up')
                .addClass('incomplete-follow-up warning')
                .html('<i class="far fa-pen-square"></i>Mark as incomplete');

            $.get(global.root + 'public/system/data/modal.json', function(result){
                var md = result['successChangeFollowupStatus']
                global.openModal(md.title, md.content, md.button, md.size);
                $('.modal .content i').text('completed');
            });
        }
    });
});

$(document).on('click', '.incomplete-follow-up', function(event) {
    event.preventDefault();
    var pID = localStorage.getItem('pID');
    var status = 'false';

    $.get(global.root + 'includes/query/schedule/toggle.php?id=' + pID + '&status=' + status, function(data){
        if (data != 'true') {
            console.log(data);

            $.get(global.root + 'public/system/data/modal.json', function(result){
                var md = result['databaseFailed'];
                global.openModal(md.title, md.content, md.button, md.size);
            });
        } else {
            $('.patient-info .followup-date > .link')
                .removeClass('incomplete-follow-up warning')
                .addClass('complete-follow-up')
                .html('<i class="far fa-pen-square"></i>Mark as completed');

            $.get(global.root + 'public/system/data/modal.json', function(result){
                var md = result['successChangeFollowupStatus'];
                global.openModal(md.title, md.content, md.button, md.size);
                $('.modal .content i').text('incomplete');
            });
        }
    });
});

$(document).on('click', '.followup-date .list [data-value], .followup-instruction-list .add-item, .followup-date .clear-field', function(event) {
    event.preventDefault();

    global.fStatusChange(localStorage.getItem('pID'), 'false');
});

$(document).on('click', '.followup-date .clear-field', function(event) {
    event.preventDefault();

    global.fStatusChange(localStorage.getItem('pID'), 'false');

    $('.followup-instruction-list .item').remove();

    $('.followup-instruction-list .add-item').click();
});

$(document).on('keypress', '.followup-instruction-list input[type="text"]', function() {
    global.fStatusChange(localStorage.getItem('pID'), 'false');
});
