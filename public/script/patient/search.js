$(document).on('input', 'input[name="search-patient"]', function(event) {
    localStorage.setItem('pLoaded', 0);

    var string = $(this).val();
    $('.patient-list').html('');

    setTimeout(function(){
        if (string.length <= 0) {
            global.patientViewLimit = 15;
            global.patientViewOffset = 0;

            global.reorderPatientList('id', 'DESC');
        } else {
            $.get(global.root + '/includes/query/patient/search.php?text=' + string, function(data){
                if (data.length == 0) {
                    $('.load-more-patients').html('No patient to show');
                } else {
                    $('.patient-list').html('');
                    global.getPatientList(data);
                }
            });
        }
    }, 100);
});
