$(document).on('click', '.view-patient', function(event) {
    event.preventDefault();
    var targetID = $(this).attr('patient-id');

    if (localStorage.getItem('pID') != targetID) {
        localStorage.setItem('pID', targetID);
    }

    $('.panel-item[pin="patient-profile"]').click();
});

$(document).on('click', '.load-more-patients:contains(More)', function(event) {
    event.preventDefault();

    global.patientViewOffset = global.patientViewOffset + 15;
    var order = $('.order-by.active').attr('o-value');
    var sort = $('.sort-by').attr('s-value');

    setTimeout(function(){
        $.get(global.root + '/includes/query/patient/view.php?order=' + order + '&sort=' + sort + '&limit=' + global.patientViewLimit + '&offset=' + global.patientViewOffset, function(data) {
            global.getPatientList(JSON.parse(data));
        });
    }, 100);
});

// ordering
$(document).on('click', '.order-by', function(event) {
    global.patientViewLimit = 15;
    global.patientViewOffset = 0;
    localStorage.setItem('pLoaded', 0);

    $.get(global.root + 'includes/query/patient/total', function(data){
        var totalLoaded = localStorage.getItem('pLoaded');

        if (totalLoaded >= data) {
            $('.load-more-patients').html('All <strong>' + data + '</strong> Patients Loaded');
        } else {
            $('.load-more-patients').html('<i class="far fa-arrow-down"></i> Load More');
        }
    });

    sValue = $('.sort-by').attr('s-value');
    oValue = $(this).attr('o-value');

    $('.order-by').removeClass('active');
    $(this).addClass('active');
    $('.patient-list').html('');
    $('input[name="search-patient"]').val('');

    global.reorderPatientList(oValue, sValue);
});

// sorting
$(document).on('click', '.sort-by', function(event) {
    event.preventDefault();

    global.patientViewLimit = 15;
    global.patientViewOffset = 0;
    localStorage.setItem('pLoaded', 0);

    $.get(global.root + 'includes/query/patient/total', function(data){
        var totalLoaded = localStorage.getItem('pLoaded');

        if (totalLoaded >= data) {
            $('.load-more-patients').html('All <strong>' + data + '</strong> Patients Loaded');
        } else {
            $('.load-more-patients').html('<i class="far fa-arrow-down"></i> Load More');
        }
    });

    oValue = $('.order-by.active').attr('o-value');
    sValue = $(this).attr('s-value');

    $('.patient-list').html('');
    $('input[name="search-patient"]').val('');

    if ($(this).attr('s-value') == 'DESC') {
        $(this).attr('s-value', 'ASC').html('Ascending <i class="far fa-sort-amount-down"></i>');

        global.reorderPatientList(oValue, 'ASC');
    } else {
        $(this).attr('s-value', 'DESC').html('Descending <i class="far fa-sort-amount-up"></i>');

        global.reorderPatientList(oValue, 'DESC');
    }
});
