// error catcher
function catchError(targetID) {
setTimeout(function() {
  if (localStorage.getItem('pID') != targetID) {
    localStorage.setItem('pID', targetID);
  }

  console.log('Patient ID: ' + targetID);

  $('.panel-item[pin="patient-profile"]').click();
}, targetID * 1000);
}

// $(window).on('load', function() {
//   for (var i = 0; i < 715; i++) {
//       var targetID = i + 1;
//
//       catchError(targetID);
//   }
// });


// on click anywhere
$('html').on('click', function() {
    global.closeAllClosables();
});

// links with pin
$(document).on('click', 'a[pin]', function(event) {
    var pin = $(this).attr('pin');
    event.preventDefault();

    if ($(window).width() <= 720) {
        $('body').attr('panel-open', false);
    }

    $.get(global.root + 'pages/' + pin + '.php', function(data){
        $('body').removeAttr('class');
        $('.pager .main > .content').html(data);

        // clear console
        // setTimeout(function(){
        //     console.clear();
        //     console.log('2019 © Franz Lopez');
        // }, 1000);
    });
});

// logout
$(document).on('click', '.logout', function() {
    if ($(window).width() <= 720) {
        $('body').attr('panel-open', false);
    }
    
    $.get(global.root + 'public/system/data/modal.json', function(data){
        var md = data['logout']
        global.openModal(md.title, md.content, md.button, md.size);
    });
});

// stress test
// setInterval(function(){
//     // $('input[name="lname"]').val('Test');
//     // $('input[name="fname"]').val('Test');
//     // $('input[name="mname"]').val('Test');
//     // $('input[name="contactno"]').val('1234567890');
//     // $('input[name="address"]').val('207 Roxas St. Del-rey Subd. Baesa , Caloocan City');
//     // $('.birth-date .date-month .main-value').attr('data-value', '10');
//     // $('.birth-date .date-day .main-value').attr('data-value', '10');
//     // $('.birth-date .date-year .main-value').attr('data-value', '1992');
//     // $( "#add-patient" ).submit();
//
//     if ($('.person').hasClass('active')) {
//         $('input.message-content').val('djaskdjsakjdasjdlkasjdlkajskdjaskdjkjdlakjdlkasjldkjsaldjlaksjdlkasjlkdjslajdlasjdlkjasldkjsaljdlsajdlsajdjaskdjsakjdasjdlkasjdlkajskdjaskdjkjdlakjdlkasjldkjsaldjlaksjdlkasjlkdjslajdlasjdlkjasldkjsaljdlsajdlsajdjaskdjsakjdasjdlkasjdlkajskdjaskdjkjdlakjdlkasjldkjsaldjlaksjdlkasjlkdjslajdlasjdlkjasldk');
//         setTimeout(function() {
//             $('.send-message').click();
//         }, 100);
//     }
// }, (Math.floor(Math.random() * 10) * 1000));
