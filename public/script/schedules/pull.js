// pull schedules
setInterval(function(){
    if (!$('body').hasClass('login-page')) {
        if (!$('body').hasClass('schedule-fetching')) {
            $('body').addClass('schedule-fetching');
        	// get schedules total
            var atype = $('.user-info').attr('atype');

            // get result count from storage
            var pulledValue = localStorage.getItem('pulledSchedule');
            // console.log('date: ' + global.getDate('-', 'ymd'));
            $.get(global.root + 'includes/query/schedule/total.php?date=' + global.getDate('-', 'ymd'), function(data){
                if (data != 0) {
                    // add sidepanel badge
                    $('.side-panel a[pin="schedules"] .counter').remove();
                    $('.side-panel a[pin="schedules"]').append('<span class="counter">' + data + '</span>');

                    var total = data;
                    var result = data + global.getDate('-', 'ymd')
                    // if has an update
                    if (result != pulledValue) {
                        // delete in the database first
                        $.get(global.root + 'includes/query/schedule/delete.php?date=' + global.getDate('-', 'ymd'), function(response){
                            if (response == 'true') {
                                // if not in the database
                                $.get(global.root + 'includes/query/schedule/check.php?date=' + global.getDate('-', 'ymd'), function(data){
                                    if (data == 0) {
                                        var content = 'Scheduled follow up for <i class="fn-date">' + global.getDate('-', 'ymd') + '</i>';

                                        global.pushNotification('system', 'standard', content);

                                        // fetch notifications
                                        global.reloadNotification();

                                        // set result count in storage
                                        localStorage.setItem('pulledSchedule', result);

                                        // reload schedule list
                                        // global.getFollowUpList(global.getDate('-', 'ymd'));
                                        if ($('body').hasClass('schedules-page')) {
                                          $('.panel-item[pin="schedules"]').click();
                                        }

                                    	// get unread notification total
                                        global.getUnreadNotifCount();

                                        // play sound
                                        if ($('#notification-audio').length > 0) {
                                            $('#notification-audio').get(0).play();
                                        }
                                    }
                                });
                            }
                        });
                    }
                } else {
                    // clear sidepanel and notification
                    $.get(global.root + 'includes/query/schedule/delete.php?date=' + global.getDate('-', 'ymd'), function(response){
                        localStorage.removeItem('pulledSchedule');
                        $('.side-panel a[pin="schedules"] .counter').remove();
                    });
                }

                setTimeout(()=> {
                    $('body').removeClass('schedule-fetching');
                }, (Math.floor(Math.random() * 10) * 5000));
            });
        }
    }
}, (Math.floor(Math.random() * 10) * 5000));
