<style>
html {
    overflow-x: hidden;
    overflow-y: auto;
    width: 100%;
    position: relative;
}

body {
    overflow-x: hidden;
    overflow-y: auto;
    background-color: #e4e5e6;
    margin: 0;
    font-size: <?=$bodyFontSize?>;
    position: relative;
    width: 100%;
    font-family: <?=$primaryFont?>;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

* {
    box-sizing: border-box;
}

a {
    outline: 0;
    text-decoration: none;
}

.hidden {
    display: none !important;
}

.clear {
    clear: both;
}

.separator {
	display: block;
	height: 1px;
	background-color: #000;
	opacity: 0.1;
	margin-bottom: 25px;
	margin-top: 15px;
}

.transitionize-fast {
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

.transitionize-faster {
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    -o-transition: all 0.2s ease;
    transition: all 0.2s ease;
}

.transitionize-slow {
    -webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    transition: all 1s ease;
}

.transitionize-slower {
    -webkit-transition: all 1.5s ease;
    -moz-transition: all 1.5s ease;
    -o-transition: all 1.5s ease;
    transition: all 1.5s ease;
}

.link {
    color: <?=$primaryColor?>;
    font-weight: 600;
}

.link.warning {
    color: <?=$red?>;
}

.link > [data-icon] {
    margin-right: 5px;
}

.button.inline {
	display: inline-block;
	background-color: <?=$primaryColor?>;
	padding: 10px 15px;
	border-radius: 5px;
	color: #fff;
}

.button.inline.warning {
	background-color: <?=$red?>;
}

.button > i, .button > svg {
	margin-right: 5px;
	font-size: 0.8em;
	position: relative;
	top: -1px;
}

.title-separator {
	display: block;
	padding: 0 25px;
	font-size: 1.2em;
	margin: 25px auto;
	position: relative;
	max-width: 980px;
}

.title-separator > a {
	position: absolute;
	text-decoration: none;
	right: 25px;
	font-size: 0.8em;
	top: 1px;
    color: <?=$primaryColor?>;
	background-color: #e3e4e5;
	padding: 0 0 0 15px;
}

.title-separator > a > [data-icon] {
	margin-left: 5px;
	position: relative;
	top: 0.5px;
}

.button-separator {
	display: block;
	text-align: center;
	text-transform: uppercase;
	text-decoration: none;
	color: <?=$primaryColor?>;
	margin-top: 20px;
	padding: 15px 25px;
	letter-spacing: 1px;
}


/* Forms */
.form form {
	padding: 12px 25px 0px 25px;
}

.form h1 {
	margin: 1px 0px 0 0px;
	font-weight: 500;
	font-size: 1.3em;
	color: #606060;
	padding: 30px;
	background-color: #e9e9e9;
}

.form .icon {
	position: absolute;
	left: 20px;
	top: 50%;
	transform: translateY(-50%);
	opacity: 0.5;
    pointer-events: none;
}

.form .icon.right {
    left: auto;
	right: 20px;
}

input[type="text"],
input[type="password"],
input[type="email"],
input[type="date"],
textarea {
	background-color: transparent;
	border-top: 0;
	border-left: 0;
	border-right: 0;
	display: block;
	width: 100%;
	border-bottom: 1px solid #dfdfdf;
	padding: 20px 40px 20px 20px;
	margin-bottom: 10px;
	font-size: 1em;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
}

.form input[type="submit"] {
	background-color: <?=$primaryColor?>;
	color: #fff;
	font-size: 1em;
	border: 0;
	padding: 20px;
	display: block;
	width: 100%;
	border-radius: 5px;
	margin-top: 20px;
	text-transform: uppercase;
	font-weight: 500;
	letter-spacing: 2px;
    margin-bottom: 30px;
    cursor: pointer;
}

.form-container {
	position: relative;
	background-color: #fbfbfb;
	border-radius: 5px;
	box-shadow: 10px 10px 20px -10px rgba(0, 0, 0, 0.1);
	margin: 50px auto 0px auto;
	display: block;
	padding-bottom: 5px;
	padding-top: 1px;
	max-width: 980px;
}

.form-title {
	font-size: 1.5em;
	font-weight: 500;
	opacity: 0.8;
	margin-bottom: 0;
	padding-top: 30px;
	color: <?=$primaryColor?>;
}

@media only screen and (max-width: 720px) {
    .form-title {
    	font-size: 1.1em;
    	font-weight: 600;
    }
}

.form-title:first-child {
	padding-top: 0;
}

.form .item-sub {
	padding-left: 20px;
}

.form .item-sub > h3 {
	display: block;
	font-weight: 400;
	font-size: 1.2em;
	opacity: 0.6;
	margin-bottom: 0;
}

/* Table */
.table-container {
	position: relative;
	background-color: #fbfbfb;
	border-radius: 5px;
	box-shadow: 10px 10px 20px -10px rgba(0, 0, 0, 0.1);
	margin: 50px auto 0px auto;
	display: block;
	padding: 15px 0;
	max-width: 980px;
    overflow-x: auto;
}

table {
	width: 100%;
	border-collapse: collapse;
}

th {
	padding: 15px;
}

tr:first-child {
	font-size: 0.8em;
	text-transform: capitalize;
	font-weight: normal;
	opacity: 0.5;
}

tr:first-child > th {
	border-bottom: 1px solid rgba(0, 0, 0, 0.1);
}

tr:hover > td {
	background-color: rgba(0, 0, 0, 0.05);
}

td {
	padding: 15px;
	opacity: 0.7;
}

td, th {
    white-space: nowrap;
}

table .trn_date {
	text-align: right;
}

table .id {
	text-align: left;
}

table .fname,
table .mname,
table .lname {
	text-align: center;
}

table .functions {
	text-align: right;
}

table .functions > a {
	background-color: <?=$primaryColor?>;
	padding: 5px 10px;
	border-radius: 5px;
	text-decoration: none;
	color: #fff;
	box-shadow: 2px 2px 0px 1px #1f6d29;
	margin-left: 5px;
	display: inline-block;
	width: 35px;
	text-align: center;
}

table .functions > a:first-child {
	margin-left: 0;
}

table .functions > a.edit-patient {
	background-color: #f68002;
	box-shadow: 2px 2px 0px 1px #d66000;
}

table .functions > a.delete-patient {
	background-color: #cb2229;
	box-shadow: 2px 2px 0px 1px #ab0209;
}


/* Side Panel */

.side-panel {
	z-index: 100;
	background-color: #fff;
	position: fixed;
	top: 0;
	left: 0;
	bottom: 0;
	width: 250px;
	box-shadow: 2px 0px 20px -10px rgba(0, 0, 0, 0.5);
    transition: max-width 0.1s ease-in-out;
}

[panel-open="false"] .side-panel {
    width: 70px;
}


@media only screen and (max-width: 720px) {
    .side-panel {
    	transform: translateX(-100%);
        transition: transform 0.1s ease-in-out;
    }

    body[panel-open="true"]:not(.login-page) {
        background-color: #949596;
    }

    [panel-open="true"] .side-panel {
    	transform: none;
    }

    body[panel-open="true"]:not(.login-page) > *:not(.header):not(.side-panel):not(.pager),
    body[panel-open="true"]:not(.login-page) .pager .content,
    body[panel-open="true"]:not(.login-page) .pager .page-heading > *,
    body[panel-open="true"]:not(.login-page) .header .menu-item[title="Notifications"] {
    	display: none !important;
    }
}

.panel-item {
	text-decoration: none;
	font-size: 1em;
	color: #5f5f5f;
	padding: 10px 25px;
	display: block;
	border-left: 3px solid transparent;
    position: relative;
}

.panel-item.active {
	border-left: 3px solid <?=$primaryColor?>;
    font-weight: 600;
}

.panel-item:hover {
	background-color: rgba(0, 0, 0, 0.05);
}

.panel-item.child {
	padding-left: 35px;
}

.panel-item [data-icon] {
	opacity: 0.5;
	font-size: 0.8em;
	top: 50%;
    transform: translateY(-50%);
	position: absolute;
}

.panel-item.active [data-icon] {
	opacity: 0.8;
}

.panel-item > span {
	margin-left: 35px;
}

[panel-open="false"] .panel-item > span {
    opacity: 0;
}

.panel-item > span.counter {
	margin: 0;
	float: right;
	font-size: 0.6em;
	background-color: <?=$red?>;
	color: #fff;
	padding: 3px 7px;
	border-radius: 30px;
	position: relative;
	top: 1px;
}

/* User Info */
.user-info {
    height: 160px;
    transition: height 0.1s ease-in-out;
}

[panel-open="false"] .user-info {
    height: 70px;
}

.user-info > .image {
	background-color: #dfdfdf;
	width: 80px;
	height: 80px;
	margin: 30px auto 0px auto;
	border-radius: 100%;
	overflow: hidden;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center;
    transition: all 0.1s ease-in-out;
    position: relative;
}

[panel-open="false"] .user-info .image {
    width: 40px;
    height: 40px;
}

.user-info > .name {
	text-align: center;
	margin: 10px 0px 2.5px 0px;
	font-weight: normal;
	font-size: 1.4em;
}

.user-info > .title {
	display: block;
	text-align: center;
	font-size: 0.9em;
	margin-bottom: 20px;
}

[panel-open="false"] .user-info .name,
[panel-open="false"] .user-info .title {
    display: none;
}

[panel-open] .header,
[panel-open] .main,
[panel-open] .page-heading,
[panel-open] .header-connected-container {
    transition: padding-left 0.1s ease-in-out;
}

[panel-open="true"] .page-heading,
[panel-open="true"] .header-connected-container {
    padding-left: 275px;
}


[panel-open="false"] .page-heading,
[panel-open="false"] .header-connected-container {
    padding-left: 95px;
}

@media only screen and (max-width: 720px) {
    [panel-open="false"] .page-heading,
    [panel-open="false"] .header-connected-container {
        padding-left: 25px;
    }
}

[panel-open="true"] .header,
[panel-open="true"] .main {
    padding-left: 250px;
}

[panel-open="false"] .header,
[panel-open="false"] .main {
    padding-left: 70px;
}

@media only screen and (max-width: 720px) {
    [panel-open="false"] .header,
    [panel-open="false"] .main {
        padding-left: 0;
    }
}


/* Header */
.header {
	background-color: rgba(0, 0, 0, 0.2);
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	z-index: 99;
}

.header .menu-item {
	color: #fff;
	display: inline-block;
	padding: 17.5px 25px;
    position: relative;
}

.header .menu-item:hover {
	background-color: rgba(0, 0, 0, 0.1);
}

.header .menu-item.right {
	float: right;
}

.header .menu-item .counter {
	position: absolute;
	background-color: <?=$red?>;
	display: block;
	padding: 3px 0;
	width: 18px;
	font-size: 0.6em;
	border-radius: 100%;
	text-align: center;
	top: 13px;
	right: 13px;
}

.header-connected-container {
	background-color: <?=$primaryColor?>;
	background-image: linear-gradient(141deg, <?=$lighterPrimaryColor?> 0%, <?=$primaryColor?> 51%, <?=$darkerPrimaryColor?> 75%);
	position: fixed;
	top: 137px;
	width: 100%;
	left: 0;
	padding: 0px 25px 0px 275px;
    z-index: 1;
}

@media only screen and (max-width: 720px) {
    .header-connected-container {
    	top: 134px;
    }
}

.header-connected-container .form {
	position: relative;
    background-color: #fff;
    border-radius: 5px;
}


/* Footer */
.credit {
    position: fixed;
    color: #000;
    bottom: 20px;
    left: 25px;
    opacity: 0.2;
    pointer-events: none;
    text-transform: uppercase;
    font-size: 0.8em;
    z-index: 9999;
}

@media only screen and (max-width: 720px) {
    .credit {
        display: none !important;
    }
}


/* Clear Fields */
.clear-field {
	position: absolute;
	display: inline-block;
	text-align: center;
	background-color: <?=$red?>;
	color: #fff;
	top: -25px;
	right: 0;
	border-radius: 5px;
	padding: 5px 10px;
	font-size: 0.8em;
	text-transform: uppercase;
	letter-spacing: 1px;
}

.clear-field [data-icon] {
	margin-right: 5px;
}


/* Selection List */
.selection-list {
	background-color: transparent;
	border-top: 0;
	border-left: 0;
	border-right: 0;
	display: block;
	width: 100%;
	border-bottom: 1px solid #dfdfdf;
	padding: 20px;
	margin-bottom: 10px;
    font-size: 1em;
	color: #000;
	font-weight: 400;
    position: relative;
   -moz-user-select: none;
   -khtml-user-select: none;
   -webkit-user-select: none;
   -ms-user-select: none;
   user-select: none;
   -webkit-transition: all 0.2s ease;
   -moz-transition: all 0.2s ease;
   -o-transition: all 025s ease;
   transition: all 0.2s ease;
}

.selection-list.active {
    z-index: 5;
}

.selection-list:hover {
    background-color: #efefef;
}

.selection-list .main-value {
    color: #757575;
}

.selection-list.changed .main-value {
    color: #000;
}

.selection-list .fa-angle-down {
    opacity: 0.25;
}

.selection-list,
.selection-list > span,
.selection-list > span > span,
.selection-list > span > svg {
    cursor: pointer;
}

.selection-list > span > span {
    margin-right: 10px;
}

.selection-list > span > svg {
    float: right;
    top: 2px;
    position: relative;
}

.selection-list .button > span {
    display: inline-block !important;
}

.selection-list .small-title {
    font-size: 0.75em;
    margin: 10px 10px 10px 10px;
    opacity: 0.35;
    text-transform: uppercase;
}

.selection-list .carret {
	width: 20px;
	position: absolute;
	z-index: 1;
    top: 30px;
	left: 30px;
    opacity: 0;
    -webkit-transition: all 0.1s ease;
    -moz-transition: all 0.1s ease;
    -o-transition: all 0.1s ease;
    transition: all 0.1s ease;
}

.selection-list.active .carret {
	top: 43px;
    opacity: 1;
}

.selection-list > .list {
    position: absolute;
    top: 30px;
    left: 0;
    background-color: #fff;
    border: 2.5px solid #dfdfdf;
    border-radius: 5px;
    height: 140px;
    width: 100%;
    max-width: 200px;
    overflow: hidden;
    opacity: 0;
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    -o-transition: all 025s ease;
    transition: all 0.2s ease;
    box-shadow: 10px 10px 15px -3px rgba(0, 0, 0, 0.2);
}

.selection-list.active > .list {
    top: 50px;
    opacity: 1;
}

.selection-list > .list > .overflow {
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    overflow-y: auto;
    width: calc(100% + 65px);
    padding-right: 50px;
}

.selection-list > .list > .overflow > span {
    display: block;
    padding: 10px;
    cursor: pointer;
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    -o-transition: all 025s ease;
    transition: all 0.2s ease;
}

.selection-list > .list > .overflow > span:hover {
    background-color: <?=$primaryColor?>;
    color: #fff;
}


/* Check List */
.checklist {
	padding-top: 15px;
}

.checklist > .item {
	display: block;
	text-decoration: none;
	color: #757575;
	padding: 7.5px 20px;
}

.checklist > .item > i,
.checklist > .item > svg {
	margin-right: 5px;
}

.checklist .item-below {
	padding-left: 20px;
}


/* Patient List */
[data-type="patient"] {
	background-color: #fff;
	padding: 15px 20px;
	display: block;
	color: #000;
	text-decoration: none;
	border-bottom: 1px rgba(0, 0, 0, 0.1) solid;
    position: relative;
}

[data-type="patient"]:hover {
	background-color: rgba(255, 255, 255, 0.9);
}

[data-type="patient"]:last-child {
	border-bottom: none;
}

[data-type="patient"] .image {
	width: 65px;
	height: 65px;
	background-repeat: no-repeat;
	background-size: cover;
	border-radius: 5px;
	float: left;
	margin-right: 15px;
	background-color: <?=$primaryColor?>;
	background-image: linear-gradient(141deg, <?=$lighterPrimaryColor?> 0%, <?=$primaryColor?> 51%, <?=$darkerPrimaryColor?> 75%);
}

[data-type="patient"] .name {
	font-size: 1.2em;
	position: relative;
	top: -1px;
	margin-bottom: 3px;
	padding-left: 80px;
    padding-right: 15px;
    text-transform: uppercase;
}

@media only screen and (max-width: 720px) {
    [data-type="patient"] .name {
    	font-size: 1em;
    	font-weight: 600;
    }
}

[data-type="patient"] .address {
	font-size: 0.8em;
	opacity: 0.6;
	padding-left: 80px;
    padding-right: 15px;
	margin-top: 5px;
}

[data-type="patient"] .address [data-icon] {
	margin-right: 3.5px;
    opacity: 0.4;
}

[data-type="patient"] .age {
	font-size: 0.8em;
	opacity: 0.5;
	font-weight: 600;
	margin-top: 8px;
	padding-left: 80px;
    padding-right: 15px;
}

[data-type="patient"] .go {
	position: absolute;
	right: 20px;
	top: 50%;
	transform: translateY(-50%);
	color: rgba(0, 0, 0, 0.2);
}


/* Tabs */
.tab-container .item {
	display: inline-block;
	text-align: center;
	float: left;
	padding: 15px 10px;
	color: rgba(255, 255, 255, 0.6);
	text-transform: uppercase;
	font-weight: 600;
	font-size: 0.9em;
	border-bottom: 2px solid transparent;
}

.tab-container.four .item {
	width: 25%;
}

.tab-container .item.active {
	color: #fff;
	border-bottom: 2px solid #fff;
}

.tab-container .item.sort-by {
	color: #fff;
}

.tab-container .item.sort-by [data-icon] {
	margin-left: 5px;
}


/* Datepicker */
.date-container {
	position: relative;
}

.date-container > div:not(.clear) {
	float: left;
	width: 33.33%;
}

@media only screen and (max-width: 720px) {
    .date-container > div:not(.clear) {
    	float: none;
    	width: 100%;
    }
}


/* OB History Spaces */
.ob-history-spaces > .selection-list {
	float: left;
	width: 25%;
}

@media only screen and (max-width: 720px) {
	.ob-history-spaces > .selection-list {
		width: 100%;
	}
}


/* Item List */
.item-list .item {
	position: relative;
}

.item-list .add-item {
	color: #000;
	padding: 10px 20px;
	display: block;
	opacity: 0.5;
}

.item-list .add-item:hover {
	opacity: 1;
}

.item-list .add-item [data-icon] {
	margin-right: 15px;
	opacity: 0.8;
}

.item-list .fa-times {
	position: absolute;
	right: 20px;
    top: 50%;
    transform: translateY(-50%);
    cursor: pointer;
    opacity: 0;
}

.item-list .item:hover .fa-times {
    opacity: 0.4;
}

.item-list .item:hover .fa-times:hover {
    opacity: 1;
}

.item-list .item:first-child .fa-times {
    display: none;
}

/* Fonts */
@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 100;
  src: local('Lato Hairline Italic'), local('Lato-HairlineItalic'), url('public/system/fonts/lato/S6u-w4BMUTPHjxsIPx-mPCLC79U11vU.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 100;
  src: local('Lato Hairline Italic'), local('Lato-HairlineItalic'), url('public/system/fonts/lato/S6u-w4BMUTPHjxsIPx-oPCLC79U1.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 300;
  src: local('Lato Light Italic'), local('Lato-LightItalic'), url('public/system/fonts/lato/S6u_w4BMUTPHjxsI9w2_FQftx9897sxZ.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 300;
  src: local('Lato Light Italic'), local('Lato-LightItalic'), url('public/system/fonts/lato/S6u_w4BMUTPHjxsI9w2_Gwftx9897g.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 400;
  src: local('Lato Italic'), local('Lato-Italic'), url('public/system/fonts/lato/S6u8w4BMUTPHjxsAUi-qNiXg7eU0.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 400;
  src: local('Lato Italic'), local('Lato-Italic'), url('public/system/fonts/lato/S6u8w4BMUTPHjxsAXC-qNiXg7Q.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 700;
  src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url('public/system/fonts/lato/S6u_w4BMUTPHjxsI5wq_FQftx9897sxZ.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 700;
  src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url('public/system/fonts/lato/S6u_w4BMUTPHjxsI5wq_Gwftx9897g.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 900;
  src: local('Lato Black Italic'), local('Lato-BlackItalic'), url('public/system/fonts/lato/S6u_w4BMUTPHjxsI3wi_FQftx9897sxZ.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: italic;
  font-weight: 900;
  src: local('Lato Black Italic'), local('Lato-BlackItalic'), url('public/system/fonts/lato/S6u_w4BMUTPHjxsI3wi_Gwftx9897g.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 100;
  src: local('Lato Hairline'), local('Lato-Hairline'), url('public/system/fonts/lato/S6u8w4BMUTPHh30AUi-qNiXg7eU0.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 100;
  src: local('Lato Hairline'), local('Lato-Hairline'), url('public/system/fonts/lato/S6u8w4BMUTPHh30AXC-qNiXg7Q.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 300;
  src: local('Lato Light'), local('Lato-Light'), url('public/system/fonts/lato/S6u9w4BMUTPHh7USSwaPGQ3q5d0N7w.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 300;
  src: local('Lato Light'), local('Lato-Light'), url('public/system/fonts/lato/S6u9w4BMUTPHh7USSwiPGQ3q5d0.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url('public/system/fonts/lato/S6uyw4BMUTPHjxAwXiWtFCfQ7A.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url('public/system/fonts/lato/S6uyw4BMUTPHjx4wXiWtFCc.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 700;
  src: local('Lato Bold'), local('Lato-Bold'), url('public/system/fonts/lato/S6u9w4BMUTPHh6UVSwaPGQ3q5d0N7w.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 700;
  src: local('Lato Bold'), local('Lato-Bold'), url('public/system/fonts/lato/S6u9w4BMUTPHh6UVSwiPGQ3q5d0.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 900;
  src: local('Lato Black'), local('Lato-Black'), url('public/system/fonts/lato/S6u9w4BMUTPHh50XSwaPGQ3q5d0N7w.woff2') format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 900;
  src: local('Lato Black'), local('Lato-Black'), url('public/system/fonts/lato/S6u9w4BMUTPHh50XSwiPGQ3q5d0.woff2') format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
</style>
