<?php include '../includes/variable.php'; ?>

<div class="form-container" style="background-color: transparent; box-shadow: none;">
	<div class="left-container">
		<span class="day-today"></span>
		<span class="date-today"></span>
	</div>

	<div class="right-container">
		<div class="schedule-list" style="margin-bottom: 30px;"></div>
	</div>

	<div class="clear"></div>
</div>

<script type="text/javascript">
	setTimeout(function() {
		// set start data
		global.setInfo('schedules', true);
	    $('.page-heading').append('<a class="button" href="#" pin="schedules"><i class="far fa-sync-alt"></i> Refresh</a>');

		$('.day-today').text(global.getWeekDay());
		$('.date-today').text(global.getDate('/', 'dfm'));
	    $('.schedule-list').empty();
	    $('body').removeClass('single-schedule-fetching');

		// console.log(global.newDate.getFullYear() + '-' + global.month + '-' + global.day);
		var monthLength = global.month.toString().length;
		var dayLength = global.day.toString().length;

		for (var i = global.day; i < 32; i++) {
			var currentCount = i + 1;
			var newDay = i;
			var newMonth = global.month;
			var forDay = new Date(newMonth + '/' + newDay + '/' + global.newDate.getFullYear())
	        var weekday=new Array(7);
	        weekday[0]="Sunday";
	        weekday[1]="Monday";
	        weekday[2]="Tuesday";
	        weekday[3]="Wednesday";
	        weekday[4]="Thursday";
	        weekday[5]="Friday";
	        weekday[6]="Saturday";
	        var dayName = weekday[forDay.getDay()];

			if (newDay.toString().length == 1) {
				newDay = '0' + i;
			}

			if (monthLength == 1) {
				newMonth = '0' + global.month;
			}

			global.getFollowUpList(global.newDate.getFullYear() + '-' + newMonth + '-' + newDay, newDay, dayName);

			if (currentCount == 32) {
				global.getNextMonthFollowUpList();
			}
		}
	}, 1000);
</script>

<style>
.schedule-list .new-month {
	border-top: 1px solid rgba(0, 0, 0, 0.05);
	display: block;
	font-weight: 300;
	margin-top: 30px;
	padding-top: 30px;
	font-size: 1.5em;
}

.left-container {
	padding: 25px;
	float: left;
	width: 40%;
}

.right-container {
	padding: 25px;
	float: right;
	width: 60%;
}

@media only screen and (max-width: 720px) {
	.left-container, .right-container {
		float: none;
		width: 100%;
	}

	.left-container {
		padding: 0 25px;
	}
}

.day-today {
	display: block;
	font-size: 1.5em;
	font-weight: 300;
	opacity: 0.5;
}

.date-today {
	font-size: 2em;
	font-weight: 300;
	display: block;
	color: <?=$primaryColor?>;
}

.schedule-list > a {
	box-shadow: 10px 10px 20px -10px rgba(0, 0, 0, 0.1);
	border-radius: 5px;
	margin-bottom: 10px;
}

.schedule-list > a .title {
	display: block;
	opacity: 0.5;
	margin-bottom: 5px;
	text-transform: uppercase;
	font-size: 0.8em;
	text-overflow: ellipsis;
	overflow: hidden;
	width: calc(100% - 90px);
	white-space: nowrap;
}

.schedule-list > a .name {
	display: block;
	margin-bottom: 5px;
	opacity: 0.8;
	padding-left: 0;
	margin-top: 7.5px;
}

.schedule-list > a .contact {
	display: block;
	font-size: 0.9em;
	opacity: 0.5;
}

.schedule-list > a .contact [data-icon] {
	color: <?=$primaryColor?>;
	margin-right: 10px;
}

.schedule-list > a .status {
	position: absolute;
	right: 20px;
	top: 15px;
	font-size: 0.65em;
	text-transform: uppercase;
	padding: 3.5px 7px;
	background-color: <?=$primaryColor?>;
	color: #fff;
	border-radius: 15px;
	font-weight: 900;
	letter-spacing: 0.5px;
}

.schedule-list > a .status.not {
	background-color: <?=$red?>;
}

.schedule-list .day-sorter {
	font-weight: 300;
	font-size: 1.3em;
	margin-top: 30px;
	margin-bottom: 20px;
	border-top: 1px solid rgba(0, 0, 0, 0.05);
	padding-top: 20px;
}

.schedule-list .day-sorter:first-child {
	margin-top: 0;
	border-top: none;
	padding-top: 0;
}
</style>
