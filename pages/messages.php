<?php include '../includes/variable.php'; ?>

<div class="people-list">
    <a href="#" class="new-conversation">
        <span class="image">
            <i class="fal fa-plus"></i>
        </span>

        <span class="name" style="margin-top: 15px;">New Conversation</span>

        <i class="far fa-angle-right"></i>

        <div class="clear"></div>
    </a>

    <div class="list"></div>
</div>

<div class="messages-container"></div>

<div class="message-box">
    <input type="text" class="message-content" placeholder="Write a message ..." style="margin-bottom: 0;border: none;">

    <a href="#" class="send-message"><i class="fal fa-paper-plane"></i></a>
</div>

<script>
	setTimeout(function() {
	    // set start data
		global.setInfo('messages', true);
	    $('.page-heading').append('<a class="button" href="#" pin="messages"><i class="far fa-sync-alt"></i> Refresh</a>');

	    // get user id
	    var uID = <?php session_start(); echo $_SESSION['id'] ?>;

		// get messages total
	    $.get(global.root + 'includes/query/message/total.php?mto=' + uID, function(data){
	        localStorage.setItem('pulledMessages', data);
	    });

	    global.reloadMessages();
	}, 1000);
</script>

<style>
.people-list {
	position: fixed;
	top: 137px;
	right: 0;
	background-color: rgba(255, 255, 255, 0.7);
	left: 0;
	width: 100%;
	max-width: 520px;
	bottom: 0;
    padding-left: 250px;
}

[panel-open="false"] .people-list {
    padding-left: 70px;
    width: 380px;
}

@media only screen and (max-width: 720px) {
    [panel-open="false"] .people-list {
        padding-left: 0;
        width: 70px;
    }
}

.people-list .person,
.people-list .new-conversation {
	display: block;
	padding: 10px 25px;
	text-decoration: none;
	position: relative;
	border-bottom: 1px solid rgba(0, 0, 0, 0.1);
	border-left: 3px solid transparent;
}

.people-list .person:hover,
.people-list .new-conversation:hover {
	background-color: rgba(255, 255, 255, 1);
}

.people-list .person.active {
	background-color: rgba(255, 255, 255, 1);
	border-left: 3px solid <?=$primaryColor?>;
}

.people-list .person .image,
.people-list .new-conversation .image {
	width: 50px;
	height: 50px;
	display: block;
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	border-radius: 100%;
	float: left;
	margin-right: 10px;
    position: relative;
}

@media only screen and (max-width: 720px) {
    .people-list .person .image,
    .people-list .new-conversation .image {
    	left: -18px;
    }
}

.people-list .new-conversation .image {
	background-color: <?=$primaryColor?>;
	background-image: linear-gradient(141deg, <?=$lighterPrimaryColor?> 0%, <?=$primaryColor?> 51%, <?=$darkerPrimaryColor?> 75%);
    position: relative;
}

.people-list .new-conversation .image > [data-icon] {
	color: #fff;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}

.people-list .person .name,
.people-list .new-conversation .name {
	display: block;
	font-size: 1.1em;
	font-weight: 400;
	color: rgba(0, 0, 0, 0.8);
	margin-top: 3px;
	text-overflow: ellipsis;
	width: calc(90% - 60px);
	white-space: nowrap;
	overflow: hidden;
}

.people-list .person[status="unread"] .name {
	font-weight: 600;
}

.people-list .person .excerpt {
	color: rgba(0, 0, 0, 0.3);
	margin-top: 3px;
	display: block;
	text-overflow: ellipsis;
	width: calc(90% - 60px);
	white-space: nowrap;
	overflow: hidden;
}

.people-list .person[status="unread"] .excerpt {
	font-style: italic;
}

.people-list .person > [data-icon],
.people-list .new-conversation > [data-icon] {
	color: rgba(0, 0, 0, 0.2);
	position: absolute;
	top: 30px;
	right: 25px;
}

@media only screen and (max-width: 720px) {
    .people-list .person > [data-icon],
    .people-list .new-conversation > [data-icon] {
        display: none;
    }
}

.messages-container {
	position: fixed;
	right: 0;
	left: 520px;
	top: 137px;
	padding: 25px 25px 90px 25px;
	bottom: 0;
	overflow-y: auto;
	overflow-x: hidden;
}

[panel-open="false"] .messages-container {
	left: 380px;
}

@media only screen and (max-width: 720px) {
    [panel-open="false"] .messages-container {
    	left: 70px;
    }
}

.message-item {
	margin-bottom: 15px;
	border-radius: 5px;
	padding: 10px 20px;
    max-width: 100%;
    word-wrap: break-word;
}

.message-item[sender="me"] {
	float: right;
	background-color: #fff;
	text-align: right;
}

.message-item[sender="other"] {
	float: left;
	background-color: <?=$primaryColor?>;
	background-image: linear-gradient(141deg, <?=$lighterPrimaryColor?> 0%, <?=$primaryColor?> 51%, <?=$darkerPrimaryColor?> 75%);
	text-align: left;
    color: #fff;
}

.message-item .date {
	display: block;
	font-size: 0.7em;
	opacity: 0.5;
	margin-top: 7px;
}
.messages-container > .message-item:last-child {
    margin-bottom: 120px;
}

.message-box {
	background-color: #fff;
	position: fixed;
	z-index: 1;
	bottom: 0;
	right: 0;
	left: 520px;
	padding: 10px 25px;
	border-top: 1px solid rgba(0, 0, 0, 0.2);
}

[panel-open="false"] .message-box {
	left: 380px;
}

@media only screen and (max-width: 720px) {
    [panel-open="false"] .message-box {
    	left: 70px;
    }
}

.message-box > input {
	width: 100%;
	max-width: 90%;
}

.send-message {
	position: absolute;
	right: 25px;
	top: 50%;
    transform: translateY(-50%);
	font-size: 1.2em;
	color: rgba(0, 0, 0, 0.5);
    margin-top: 5px;
}

.send-message {
	position: absolute;
	right: 25px;
	top: 35px;
	font-size: 1.2em;
	color: rgba(0, 0, 0, 0.5);
}

.send-message:hover {
	color: <?=$primaryColor?>;
}

.contact-item {
	text-decoration: none;
	color: rgba(0, 0, 0, 0.6);
	display: block;
	width: calc(100% + 50px);
	margin-left: -25px;
	padding: 10px 25px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    position: relative;
}

.contact-item:last-child {
	margin-bottom: 0;
    border-bottom: none;
}

.contact-item .name {
	display: block;
	padding-top: 8px;
	padding-left: 50px;
}

.contact-item .image {
	width: 40px;
	height: 40px;
	display: block;
	background-size: cover;
	border-radius: 100%;
	float: left;
}

.contact-item > [data-icon] {
	color: rgba(0, 0, 0, 0.2);
	position: absolute;
	top: 23px;
	right: 25px;
}
</style>
