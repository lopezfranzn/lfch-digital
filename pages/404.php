<?php include '../includes/head.php'; ?>

<div class="container-fof">
    <h1>404</h1>
    <span class="subtitle">Page not found</span>
    <i>Redirecting you into the right place ...</i>
</div>

<style>
.container-fof {
	text-align: center;
	position: fixed;
	top: 50%;
	width: 100%;
	transform: translateY(-50%);
}

.container-fof h1 {
	margin: 0;
	font-size: 10em;
	opacity: 0.2;
}

.container-fof .subtitle {
	text-transform: uppercase;
	letter-spacing: 5px;
	font-size: 1.4em;
    color: <?=$primaryColor?>;
	font-weight: 300;
    display: block;
    margin-bottom: 20px;
}

.container-fof i {
	opacity: 0.5;
}
</style>

<script>
	// set start data
	global.setInfo('login', false);
    localStorage.removeItem('currentPage');

	// redirect to root
    setTimeout ( function() {
        window.location.href = global.root;
    }, 3000);
</script>

<?php include $root . 'includes/footer.php'; ?>
