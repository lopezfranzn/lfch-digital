<?php include '../includes/variable.php'; ?>

<div class="form">
	<div class="form-container" style="margin-bottom: 25px;">
		<form id="edit-patient" date="<?php echo date("Y-m-d"); ?>">
			<h2 class="form-title">Full Name</h2>
			<input type="text" name="lname" placeholder="Last Name" />
			<input type="text" name="fname" placeholder="First Name" />
			<input type="text" name="mname" placeholder="Middle Name" />

			<h2 class="form-title">Birth Date</h2>
			<div class="date-container birth-date required"></div>


			<h2 class="form-title">Contact Information</h2>
			<input type="text" name="contactno" placeholder="Contact Number" />

			<input type="text" name="address" placeholder="Address" />


			<h2 class="form-title">Past Medical History</h2>
			<div class="past-med-history checklist">
				<a href="#" class="item" data-value="hypertension" check="false"><i class="far fa-square"></i> Hypertension</a>
				<a href="#" class="item" data-value="diabetes-mellitus" check="false"><i class="far fa-square"></i> Diabetes Mellitus</a>
				<a href="#" class="item" data-value="bronchial-asthma" check="false"><i class="far fa-square"></i> Bronchial Asthma</a>
				<div class="item-below hidden">
					<div class="date-container bronchial-asthma-date"></div>
				</div>
				<a href="#" class="item" data-value="surgery" check="false"><i class="far fa-square"></i> Surgery</a>
				<div class="item-below hidden">
					<div class="item-list surgery-list" placeholders="Enter surgery item"></div>
				</div>
			</div>


			<h2 class="form-title">Last Menstrual History</h2>
			<div class="date-container last-menstrual-date"></div>


			<h2 class="form-title">Age of Gestation</h2>
			<input type="text" name="age-gestation" placeholder="Age of Gestation" />


			<h2 class="form-title">OB History</h2>
			<div class="ob-history-field" style="position: relative">
				<div class="selection-list closable data-type numbers-collection ob-history-gravida" limit="13">
				    <span><span class="main-value">Gravida</span><i class="far fa-angle-down"></i></span>
				    <img class="carret" />
				    <div class="list" style="width: 100px; height: 140px;">
				        <div class="overflow"></div>
				    </div>
				</div>

				<div class="selection-list closable data-type numbers-collection ob-history-para" limit="13">
					<span><span class="main-value">Para</span><i class="far fa-angle-down"></i></span>
					<img class="carret" />
					<div class="list" style="width: 100px; height: 140px;">
						<div class="overflow"></div>
					</div>
				</div>

				<div class="ob-history-spaces">
					<div class="selection-list closable data-type numbers-collection" field-count="1" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="selection-list closable data-type numbers-collection" field-count="2" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="selection-list closable data-type numbers-collection" field-count="3" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="selection-list closable data-type numbers-collection" field-count="4" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="clear"></div>
				</div>

				<a href="#" class="clear-field"><i class="far fa-eraser"></i> Clear</a>
			</div>


			<h2 class="form-title">Chief Complaint</h2>
			<div class="item-list chief-complaint-list" placeholders="Enter complaint item"></div>


			<h2 class="form-title">Physical Exam</h2>
			<input type="text" name="pe-bp" placeholder="BP" />

			<input type="text" name="pe-cr" placeholder="CR" />

			<input type="text" name="pe-temp" placeholder="Temp" />

			<div class="item-sub abdomen">
				<h3>Abdomen</h3>

				<input type="text" name="pe-abdomen-fh" placeholder="FH" />

				<input type="text" name="pe-abdomen-fht" placeholder="Fht" />
			</div>

			<input type="text" name="pe-ie" placeholder="IE" />

			<input type="text" name="pe-utz" placeholder="UTZ" />

			<input type="text" name="pe-others" placeholder="Others" />


			<h2 class="form-title">Lab Results</h2>
			<div class="item-list lab-result-list" placeholders="Enter lab result item"></div>


			<h2 class="form-title">Assessment</h2>

			<div class="item-list assessment-list" placeholders="Enter assessment item"></div>


			<h2 class="form-title">Plan</h2>

			<div class="item-list plan-list" placeholders="Enter plan item"></div>


			<h2 class="form-title">Follow Up</h2>
			<div class="date-container followup-date advanced"></div>

			<div class="item-list followup-instruction-list" placeholders="Enter follow up instruction item"></div>


			<input type="submit" name="submit" value="Save" />
		</form>
	</div>
</div>

<script type="text/javascript">
	setTimeout(function() {
		// set start data
		global.setInfo('edit patient', true);
		$('a[pin="patients"]').addClass('active');
	    $('.page-heading').append('<a class="button" href="#" pin="patient-profile"><i class="far fa-arrow-left"></i> Back</a>');

		global.loadDatePicker();
		global.loadNumberCollections();
		global.loadItemList();

		setTimeout(function() {
			// get patient data
			var pID = localStorage.getItem('pID');

			$.get(global.root + 'includes/query/patient/view.php?id=' + pID, function(data){
				data = JSON.parse(data);

				// auto-generated data
				$('#edit-patient').attr('image-number', data[0].image);

				// full name
				$('input[name="lname"]').val(data[0].lname);
				$('input[name="fname"]').val(data[0].fname);
				$('input[name="mname"]').val(data[0].mname);

				// birth date
				global.extractDate(data[0].birth_date, 'birth-date');

				// contact information
				$('input[name="contactno"]').val(data[0].contact_no);
				$('input[name="address"]').val(data[0].address);

				// past medical history
				if (data[0].past_med_history != 'null') {
					var pastMedHistory = JSON.parse(data[0].past_med_history);

					if (pastMedHistory[0].hypertension != 'null') {
						global.checkBoxVal(pastMedHistory[0].hypertension, 'hypertension');
					}
					if (pastMedHistory[0].diabetesMellitus != 'null') {
						global.checkBoxVal(pastMedHistory[0].diabetesMellitus, 'diabetes-mellitus');
					}
					if (pastMedHistory[0].bronchialAsthma.value != 'null') {
						global.checkBoxVal(pastMedHistory[0].bronchialAsthma.value, 'bronchial-asthma');
					}
					if (pastMedHistory[0].bronchialAsthma.date != 'null') {
						global.checkBoxVal(pastMedHistory[0].bronchialAsthma.date, 'bronchial-asthma-date');
					}
					if (pastMedHistory[0].surgery.value != 'null') {
						global.checkBoxVal(pastMedHistory[0].surgery.value, 'surgery');
					}
					if (pastMedHistory[0].surgery.list != 'null') {
						global.extractList(pastMedHistory[0].surgery.list, 'surgery-list');
					}
				}

				// last menstrual history
				if (data[0].last_mens_history != 'null') {
					global.extractDate(data[0].last_mens_history, 'last-menstrual-date');
				}

				// age of gestation
				if (data[0].age_gestation != 'null') {
					$('input[name="age-gestation"]').val(data[0].age_gestation);
				}

				// ob history
				if (data[0].ob_history != 'null') {
					var obHistory = JSON.parse(data[0].ob_history);
				    $('.ob-history-gravida').addClass('changed').find('.main-value').attr('data-value', obHistory[0].gravida).text(obHistory[0].gravida);
				    $('.ob-history-para').addClass('changed').find('.main-value').attr('data-value', obHistory[0].para).text(obHistory[0].para);
					global.extractOBHistorySpaces(obHistory[0].digits);
				}

				// chief complaint
				if (data[0].chief_complaint != 'null') {
					var chiefComplaint = JSON.parse(data[0].chief_complaint);
					global.extractList(chiefComplaint[0].list, 'chief-complaint-list');
				}

				// plan
				if (data[0].plan != 'null') {
					var plan = JSON.parse(data[0].plan);
					global.extractList(plan[0].list, 'plan-list');
				}

				// follow up
				if (data[0].follow_up != 'null') {
					var followUp = JSON.parse(data[0].follow_up);
					global.extractDate(followUp[0].date, 'followup-date');

					if (followUp[0].list != 'null') {
						global.extractList(followUp[0].list, 'followup-instruction-list');
					}
				}

				// $('.delete-patient').attr('target-patient', pID);
			});
		}, 1000);
	}, 1000);
</script>
