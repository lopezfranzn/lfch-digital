<?php include '../includes/variable.php'; ?>

<div class="form-container patient-info" style="margin-bottom: 25px;">
	<div class="left">
		<div class="info">
			<div class="image"></div>

			<div class="full-name">
				<span class="lname"></span> <span>,&nbsp;</span> <span class="fname"></span> <span class="mname"></span>
			</div>

			<div class="side-info">
				<span class="title">Age</span>
				<span class="value age"></span>
			</div>

			<div class="side-info">
				<span class="title">Birth Date</span>
				<span class="value birth-date"></span>
			</div>

			<div class="side-info">
				<span class="title">Address</span>
				<span class="value address"></span>
			</div>

			<div class="side-info">
				<span class="title">Contact Number</span>
				<span class="value contactno"></span>
			</div>
		</div>
	</div>

	<div class="right">
		<div class="group-info past-med-history">
			<span class="title">Past Medical History</span>
			<div class="values">
				<span class="hypertension" display="false">Hypertension</span>
				<span class="diabetes-mellitus" display="false">Diabetes Mellitus</span>
				<span class="bronchial-asthma" display="false">Bronchial Asthma<i class="sub bronchial-asthma-date"></i></span>
				<span class="surgery" display="false">Surgery<ul class="sub surgery-list"></ul></span>
			</div>
		</div>

		<div class="group-info last-menstrual-date">
			<span class="title">Last Menstrual History</span>
			<span class="value"></span>
		</div>

		<div class="group-info age-gestation">
			<span class="title">Age of Gestation</span>
			<span class="value"></span>
		</div>

		<div class="group-info ob-history">
			<span class="title">OB History</span>
			<span class="value"></span>
		</div>

		<div class="group-info chief-complaint">
			<span class="title">Chief Complaint</span>
			<div class="values">
				<ul class="sub chief-complaint-list"></ul>
			</div>
		</div>

		<div class="group-info physical-exam">
			<span class="title">Physical Exam</span>
			<div class="values"></div>
		</div>

		<div class="clear"></div>

		<div class="group-info lab-result">
			<span class="title">Lab Results</span>
			<div class="values"></div>
		</div>

		<div class="clear"></div>

		<div class="group-info assessment">
			<span class="title">Assessment</span>
			<div class="values"></div>
		</div>

		<div class="clear"></div>

		<div class="group-info plan">
			<span class="title">Plan</span>
			<div class="values">
				<ul class="sub plan-list"></ul>
			</div>
		</div>

		<div class="group-info followup-group">
			<span class="title">Follow Up</span>
			<span class="value followup-date" style="margin-bottom: 10px;"></span>
			<div class="values">
				<ul class="sub followup-list"></ul>
			</div>
		</div>

		<div class="functions">
			<a href="#" class="edit-patient button inline" style="margin-right: 5px;"><i class="far fa-edit"></i> Edit</a>
			<a href="#" class="delete-patient button warning inline"><i class="far fa-trash-alt"></i> Delete</a>
		</div>
	</div>

	<div class="clear"></div>
</div>

<script>
	setTimeout(function() {
		// set start data
		global.setInfo('patient profile', true);
		$('a[pin="patients"]').addClass('active');
	    $('.page-heading').append('<a class="button" href="#" pin="patients"><i class="far fa-arrow-left"></i> Patient List</a>');

		$('.patient-info .group-info .values > span').prepend('<i class="far fa-chevron-right"></i>');

		// get patient data
		var pID = localStorage.getItem('pID');

		$.get(global.root + 'includes/query/patient/view.php?id=' + pID, function(data){
			if (data != '[]') {
				data = JSON.parse(data);
		        // auto-generated data
				$('.info .image').css('background-image', 'url(' + global.root + 'public/system/images/avatar/patients/' + data[0].image + '.jpg)');

			    // full name
				$('.lname').text(data[0].lname);
				$('.fname').text(data[0].fname);
				$('.mname').text(data[0].mname);

			    // birth date
				var patientAge = global.getAge(data[0].birth_date, global.fulldate);
				if (patientAge <= 1) {
					$('.age').text(global.getAge(data[0].birth_date, global.fulldate) + ' year old');
				} else {
					$('.age').text(global.getAge(data[0].birth_date, global.fulldate) + ' years old');
				}
				$('.birth-date').text(global.detailedDate(data[0].birth_date));

				// contact information
				$('.address').text(data[0].address);
				$('.contactno').text(data[0].contact_no);

				// past medical history
				if (data[0].past_med_history != 'null') {
		            var pastMedHistory = JSON.parse(data[0].past_med_history);
					$('.hypertension').attr('display', pastMedHistory[0].hypertension);
					$('.diabetes-mellitus').attr('display', pastMedHistory[0].diabetesMellitus);
					$('.bronchial-asthma').attr('display', pastMedHistory[0].bronchialAsthma.value);

					if (pastMedHistory[0].bronchialAsthma.date == 'null') {
						$('.bronchial-asthma-date').hide();
					} else {
						$('.bronchial-asthma-date').text(pastMedHistory[0].bronchialAsthma.date);
					}

					$('.surgery').attr('display', pastMedHistory[0].surgery.value);

					if (pastMedHistory[0].surgery.list == 'null') {
						$('.surgery-list').hide();
					} else {
						console.log('surgery list');

						var pastMedHistoryContent = JSON.stringify(pastMedHistory[0].surgery.list).replace(/SYSDQUOTE/g, '&#34;').replace(/SYSQUOTE/g, '&#39;');

						global.fetchList('surgery-list', JSON.parse(pastMedHistoryContent));
					}
				} else {
					$('.past-med-history .values').html(global.noData);
				}

				// last menstrual history
				if (data[0].last_mens_history != 'null') {
					$('.last-menstrual-date .value').text(global.detailedDate(data[0].last_mens_history));
				} else {
					$('.last-menstrual-date .value').html(global.noData);
				}

				// age of gestation
				if (data[0].age_gestation != 'null') {
					$('.age-gestation .value').text(data[0].age_gestation);
				} else {
					$('.age-gestation .value').html(global.noData);
				}

				// ob history
				if (data[0].ob_history != 'null') {
		            var obHistory = JSON.parse(data[0].ob_history);
					$('.ob-history .value').text('Gravida ' + obHistory[0].gravida
					+ ' Para ' + obHistory[0].para
					+ ' (' + obHistory[0].digits + ')');
				} else {
					$('.ob-history .value').html(global.noData);
				}

				// chief complaint
				if (data[0].chief_complaint != 'null') {
					console.log('chief complaint');

					var contents = (data[0].chief_complaint).replace(/SYSDQUOTE/g, '&#34;').replace(/SYSQUOTE/g, '&#39;');
		            var chiefComplaint = JSON.parse(contents);
					global.fetchList('chief-complaint-list', chiefComplaint[0].list);
				} else {
					$('.chief-complaint .values').html(global.noData);
				}

				// physical exam
				$.get(global.root + '/includes/query/medical-record/pull.php?pid=' + pID + '&rtype=pe&limit=' + global.medRecResultLimit + '&offset=0', function(record) {
					console.log('physical exam');

					record = JSON.parse(record);

					if (record.length == 0) {
						$('.physical-exam .values').html(global.noData + '<div class="clear"></div>');
					} else {
						for (var i = 0; i < record.length; i++) {
							var currentCount = i + 1;
							var contents = (record[i].content).replace(/SYSDQUOTE/g, '&#34;').replace(/SYSQUOTE/g, '&#39;');
	            var physicalExam = JSON.parse(contents);
	            var stamp = record[i].trn_date;

							$('.physical-exam .values').append('<div class="item">'
							+ '<span class="item-title">Physical Exam #' + currentCount + '</span>'
							+ '<span class="pe-bp" display="' + global.checkIfNull(physicalExam[0].bp) + '">BP<i class="sub">' + physicalExam[0].bp + '</i></span>'
							+ '<span class="pe-cr" display="' + global.checkIfNull(physicalExam[0].cr) + '">CR<i class="sub">' + physicalExam[0].cr + '</i></span>'
							+ '<span class="pe-temp" display="' + global.checkIfNull(physicalExam[0].temp) + '">Temp<i class="sub">' + physicalExam[0].temp + '</i></span>'
							+ '<span class="pe-abdomen-fh" display="' + global.checkIfNull(physicalExam[0].abdomen.fh) + '">Abdomen FH<i class="sub">' + physicalExam[0].abdomen.fh + '</i></span>'
							+ '<span class="pe-abdomen-fht" display="' + global.checkIfNull(physicalExam[0].abdomen.fht) + '">Abdomen Fht<i class="sub">' + physicalExam[0].abdomen.fht + '</i></span>'
							+ '<span class="pe-ie" display="' + global.checkIfNull(physicalExam[0].ie) + '">IE<i class="sub">' + physicalExam[0].ie + '</i></span>'
							+ '<span class="pe-utz" display="' + global.checkIfNull(physicalExam[0].utz) + '">UTZ<i class="sub">' + physicalExam[0].utz + '</i></span>'
							+ '<span class="pe-others" display="' + global.checkIfNull(physicalExam[0].others) + '">Others<i class="sub">' + physicalExam[0].others + '</i></span>'
							+ '<span class="pe-stamp stamp" display="true"><i class="sub">' + global.detailedDate(stamp) + '</i></span>'
							+ '<a href="#" class="rec-delete" data-delete="pe" rec-id="' + record[i].id + '">✖</a>'
							+ '<div class="clear"></div></div>');

							if (currentCount == record.length) {
								$('.physical-exam .values').append('<div class="clear"></div>');

							    // masonry
							    $('.physical-exam .values').masonry({
							        itemSelector: '.item',
							        columnWidth: 210,
							        isFitWidth: true
							    });

								global.peTotal = record.length;

								$.get(global.root + '/includes/query/medical-record/total.php?pid=' + pID + '&rtype=pe', function(total) {
									if (global.peTotal < total) {
										$('.physical-exam').append('<a href="#" class="load-more-result" data-target="pe"><i class="far fa-sync"></i>Load more</a>');
									} else {
										$('.physical-exam').append('<a href="#" class="load-more-result completed" data-target="pe">All (' + total + ') result/s loaded</a>');
									}
								});
							}
						}
					}
				});

				// lab results
				$.get(global.root + '/includes/query/medical-record/pull.php?pid=' + pID + '&rtype=lr&limit=' + global.medRecResultLimit + '&offset=0', function(record){
					console.log('lab results');
					record = JSON.parse(record);

					if (record.length == 0) {
						$('.lab-result .values').html(global.noData);
					} else {
						for (var i = 0; i < record.length; i++) {
							var currentCount = i + 1;
							var contents = (record[i].content).replace(/SYSDQUOTE/g, '&#34;').replace(/SYSQUOTE/g, '&#39;');
				            var labResult = JSON.parse(contents);
				            var stamp = record[i].trn_date;

							$('.lab-result .values').append('<div class="item">'
								+ '<span class="item-title">Lab Result #' + currentCount + '</span>'
								+ '<ul class="sub lab-result-list-' + i + '"></ul>'
								+ '<span class="lab-result-stamp stamp" display="true"><i class="sub">' + global.detailedDate(stamp) + '</i></span>'
								+ '<a href="#" class="rec-delete" data-delete="lr" rec-id="' + record[i].id + '">✖</a>'
								+ '<div class="clear"></div></div>');
							global.fetchList('lab-result-list-' + i, labResult);

							if (currentCount == record.length) {
								$('.lab-result .values').append('<div class="clear"></div>');

							    // masonry
							    $('.lab-result .values').masonry({
							        itemSelector: '.lab-result .values > .item',
							        columnWidth: 210,
							        isFitWidth: true
							    });

								lrTotal = record.length;

								$.get(global.root + '/includes/query/medical-record/total.php?pid=' + pID + '&rtype=lr', function(total) {
									if (lrTotal < total) {
										$('.lab-result').append('<a href="#" class="load-more-result" data-target="lr"><i class="far fa-sync"></i>Load more</a>');
									} else {
										$('.lab-result').append('<a href="#" class="load-more-result completed" data-target="lr">All (' + total + ') result/s loaded</a>');
									}
								});
							}
						}
					}
				});

				// assessment
				$.get(global.root + '/includes/query/medical-record/pull.php?pid=' + pID + '&rtype=at&limit=' + global.medRecResultLimit + '&offset=0', function(record){
					console.log('assessment');
					record = JSON.parse(record);

					if (record.length == 0) {
						$('.assessment .values').html(global.noData);
					} else {
						for (var i = 0; i < record.length; i++) {
							var currentCount = i + 1;
							var contents = (record[i].content).replace(/SYSDQUOTE/g, '&#34;').replace(/SYSQUOTE/g, '&#39;');
				            var assessment = JSON.parse(contents);
				            var stamp = record[i].trn_date;

							$('.assessment .values').append('<div class="item">'
								+ '<span class="item-title">Assessment #' + currentCount + '</span>'
								+ '<ul class="sub assessment-list-' + i + '"></ul>'
								+ '<span class="assessment-stamp stamp" display="true"><i class="sub">' + global.detailedDate(stamp) + '</i></span>'
								+ '<a href="#" class="rec-delete" data-delete="at" rec-id="' + record[i].id + '">✖</a>'
								+ '<div class="clear"></div></div>');
							global.fetchList('assessment-list-' + i, assessment);

							if (currentCount == record.length) {
								$('.assessment .values').append('<div class="clear"></div>');

							    // masonry
							    $('.assessment .values').masonry({
							        itemSelector: '.assessment .values > .item',
							        columnWidth: 210,
							        isFitWidth: true
							    });

								atTotal = record.length;

								$.get(global.root + '/includes/query/medical-record/total.php?pid=' + pID + '&rtype=at', function(total) {
									if (atTotal < total) {
										$('.assessment').append('<a href="#" class="load-more-result" data-target="at"><i class="far fa-sync"></i>Load more</a>');
									} else {
										$('.assessment').append('<a href="#" class="load-more-result completed" data-target="at">All (' + total + ') result/s loaded</a>');
									}
								});
							}
						}
					}
				});

				// plan
				if (data[0].plan != 'null') {
					console.log('plan');
					var contents = (data[0].plan).replace(/SYSDQUOTE/g, '&#34;').replace(/SYSQUOTE/g, '&#39;');
		            var plan = JSON.parse(contents);
					global.fetchList('plan-list', plan[0].list);
				} else {
					$('.plan .values').html(global.noData);
				}

				// follow up
				if (data[0].follow_up != 'null') {
					console.log('follow up');
					var contents = (data[0].follow_up).replace(/SYSDQUOTE/g, '&#34;').replace(/SYSQUOTE/g, '&#39;');
		            var followUp = JSON.parse(contents);

					$('.followup-date').text(global.detailedDate(followUp[0].date));

					if (data[0].follow_up_status == 'true') {
						$('.followup-date').append('<a href="#" class="link incomplete-follow-up warning"><i class="far fa-pen-square"></i>Mark as incomplete</a>');
					} else if (data[0].follow_up_status == 'false') {
						$('.followup-date').append('<a href="#" class="link complete-follow-up"><i class="far fa-pen-square"></i>Mark as completed</a>');
					}

					if (followUp[0].list != 'null') {
						global.fetchList('followup-list', followUp[0].list);
					} else {
						$('.followup-list').hide();
					}
				} else {
					$('.followup-group .values').html(global.noData);
				}

				$('.delete-patient').attr('target-patient', pID);
			}
		});
	}, 1000);
</script>

<style>
.patient-info .left {
	float: left;
	padding: 25px;
	width: 30%;
}

.patient-info .right {
	float: right;
	padding: 25px;
	width: 70%;
}

@media only screen and (max-width: 720px) {
	.patient-info .left,
	.patient-info .right {
		float: none;
		width: 100%;
	}

	.patient-info .left {
		padding-bottom: 0;
	}

	.patient-info .right {
		padding-top: 0;
	}
}

.patient-info .side-info {
	margin-bottom: 25px;
}

.patient-info .side-info .title {
	font-size: 0.8em;
	text-transform: uppercase;
	letter-spacing: 1px;
	display: block;
	margin-bottom: 5px;
	opacity: 0.5;
}

.patient-info .side-info .value {
	display: block;
	opacity: 0.6;
}

.patient-info .group-info {
	margin-bottom: 25px;
}

.patient-info .group-info .title {
	display: block;
	font-size: 1.3em;
	font-weight: 500;
	margin-bottom: 15px;
	color: <?=$primaryColor?>;
}

@media only screen and (max-width: 720px) {
	.patient-info .group-info .title {
		font-size: 1.1em;
		font-weight: 600;
	}
}

.patient-info .group-info .value {
	display: block;
	opacity: 0.6;
}

.patient-info .group-info .values > span {
	margin-bottom: 10px;
	opacity: 0.6;
	display: none;
}

.patient-info .group-info .values > span[display="true"] {
	display: block;
}

.patient-info .group-info .values [data-icon] {
	font-size: 0.4em;
	margin-right: 7.5px;
	position: relative;
	top: -3px;
	color: <?=$primaryColor?>;
}

.group-info .values > .item {
	display: block;
	float: left;
	width: calc(210px - 20px);
	border: 1px solid rgba(0, 0, 0, 0.1);
	border-radius: 10px;
	padding: 15px;
	margin: 10px;
}

.group-info .values > .item > .item-title {
	display: block;
	margin-bottom: 15px;
	font-size: 1.1em;
}

.group-info .values > .item > span:not(.item-title) {
	display: block;
	font-size: 0.9em;
	font-weight: 400;
	opacity: 0.5;
	margin-bottom: 5px;
}

.group-info .values > .item > span[display="false"] {
	display: none;
}

.group-info .values > .item > span .sub {
	opacity: 0.7;
	font-style: normal;
	margin-left: 5px;
}

.group-info .values > .item .rec-delete {
	position: absolute;
	top: -7px;
	right: -7px;
	background-color: #404040;
	color: #fff;
	padding: 1px 5px;
	border-radius: 100%;
	font-size: 0.8em;
	display: none;
}

.group-info .values > .item:hover .rec-delete {
	display: inline-block;
}

.group-info .values > .item > span.stamp {
	padding: 5px 10px;
	margin-top: 13px;
	border-radius: 30px;
	border: 1px solid rgba(0, 0, 0, 0.1);
	display: inline-block;
	font-size: 0.8em;
	opacity: 1;
}

.group-info .values > .item > span.stamp .sub {
	margin-left: 0;
}

.group-info .values > .item > ul {
	display: block;
	margin: 0;
	list-style: none;
	padding: 0;
}

.group-info .values > .item > ul .item {
	display: block;
	opacity: 0.5;
	font-size: 0.9em;
	margin-bottom: 5px;
}

.patient-info .group-info .values > span > i.sub {
	font-size: 0.8em;
	display: block;
	margin-left: 13px;
	font-style: normal;
	margin-top: 5px;
	opacity: 0.6;
}

.patient-info .group-info .values > span ul {
	list-style: none;
	margin-top: 5px;
	margin-bottom: 15px;
	padding-left: 14px;
	font-size: 0.9em;
}

.patient-info .group-info .values > span ul > li {
	margin-bottom: 5px;
	opacity: 0.6;
}

.patient-info .group-info .values > ul {
	list-style: none;
	margin-top: 5px;
	margin-bottom: 15px;
	padding-left: 0;
}

.patient-info .group-info .values > ul > li {
	margin-bottom: 5px;
	opacity: 0.6;
}

/* .patient-info .group-info .values:last-child .separator {
	opacity: 0;
	position: absolute;
} */

.patient-info .image {
	background-repeat: no-repeat;
	width: 100px;
	height: 100px;
	border-radius: 100%;
	background-size: cover;
	margin-bottom: 20px;
}

.patient-info .full-name {
	font-size: 1.3em;
	margin-bottom: 25px;
	opacity: 0.8;
	text-transform: uppercase;
}

@media only screen and (max-width: 720px) {
	.patient-info .full-name {
    	font-size: 1em;
    	font-weight: 600;
    }
}

.patient-info .physical-exam .values > span {
	float: left;
	width: 25%;
}

.patient-info .followup-date > .link {
	margin-left: 10px;
}

.load-more-result {
	display: block;
	text-align: center;
	color: <?=$primaryColor?>
}

.load-more-result [data-icon] {
	margin-right: 10px;
}
</style>
