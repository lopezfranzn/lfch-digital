<?php include '../includes/variable.php'; ?>

<div class="form-container hidden" style="text-align: center; background-color: transparent; box-shadow: none;">
	<div id="slider">
		<figure>
			<img src="/public/system/images/slides/1.jpg" alt>
			<img src="/public/system/images/slides/2.jpg" alt>
			<img src="/public/system/images/slides/3.jpg" alt>
			<img src="/public/system/images/slides/4.jpg" alt>
			<img src="/public/system/images/slides/1.jpg" alt>
		</figure>
	</div>

	<div class="stat patient-total">
		<h1>0</h1>
        <span>Total Patients <i>as of <?php echo date('F j, Y g:i A  '); ?></i></span>
		<div class="clear"></div>
	</div>

	<div class="clear"></div>
</div>

<span class="title-separator">Recently Added Patients <a href="#" pin="patients">View All <i class="far fa-angle-right"></i></a></span>

<div class="table-container" style="margin-top: 0; padding: 0; background-color: transparent; margin-bottom: 25px;">
    <div class="patient-list"></div>
</div>

<script type="text/javascript">
	setTimeout(function() {
		// set start data
		global.setInfo('dashboard', true);
	    $('.page-heading').append('<a class="button" href="#" pin="dashboard"><i class="far fa-sync-alt"></i> Refresh</a>');


		// get patient total
	    $.get(global.root + 'includes/query/patient/total', function(data){
	        $('.patient-total > h1').text(data);
	    });

		// get recently added patients
	    $.get(global.root + '/includes/query/patient/view.php?order=id&sort=DESC&limit=20&offset=0', function(data){
			data = JSON.parse(data);
	        if (data.length == 0) {
				$('.patient-list').html('<span class="no-patient">No patient to show</span>');
	        } else {
	            for (var i = 0; i < data.length; i++) {
	                var currentCount = i + 1;
	                $('.patient-list').append('<a href="#" class="' + i + ' new dashboard-patient" patient-id="' + data[i].id + '" data-type="patient"></a>');
	                $('a.dashboard-patient.' + i + '.new').append('<div class="image" style="background-image: url(' + global.root + 'public/system/images/avatar/patients/' + data[i].image + '.jpg)"</div>');
	                $('a.dashboard-patient.' + i + '.new').append('<div class="name">' + data[i].lname + ', ' + data[i].fname + ' ' + data[i].mname + '</div>');
	                $('a.dashboard-patient.' + i + '.new').append('<div class="address"><i class="far fa-map-marker-alt"></i> ' + data[i].address + '</div>');
	                $('a.dashboard-patient.' + i + '.new').append('<div class="age">' + global.getAge(data[i].birth_date, global.fulldate) + ' years old</div>');
	                $('a.dashboard-patient.' + i + '.new').append('<div class="functions"><a href="#" class="go"><i class="far fa-angle-right"></i></a><div>');

	                if (currentCount == data.length) {
	                    $('.patient-list a.new').removeAttr('class').addClass('view-patient');
	                }
	            }
	        }
	    });

		setTimeout(function () {
			if (!$('body').hasClass('login-page')) {
				if (!$('.side-panel > .user-info > .image').attr('style')) {
					location.reload();
				}
			}
		}, 3000);

		$('.form-container.hidden').removeClass('hidden');
	}, 1000);
</script>

<style>
.dashboard-page .stat {
	text-align: center;
	padding: 25px;
	display: inline-block;
	width: calc(50% - 12.5px);
	float: left;
	background-color: #fff;
	border-radius: 5px;
	overflow: hidden;
	height: 170px;
	position: relative;
	box-shadow: 10px 10px 20px -10px rgba(0, 0, 0, 0.1);
	background-size: 100%;
	background-repeat: no-repeat;
	background-position: bottom;
	background-image: url('/public/system/images/bar-graph.png');
}

@media only screen and (max-width: 720px) {
	.dashboard-page .stat {
		width: 100% !important;
		height: auto;
		padding: 7.5px 20px 20px 20px;
	}
}

.dashboard-page .stat > h1 {
	font-size: 3em;
	margin: 0;
	opacity: 0.7;
	font-weight: 300;
	float: left;
}

.dashboard-page .stat > span {
	opacity: 0.5;
	float: right;
	position: relative;
	top: 6px;
	margin-left: 9px;
}

@media only screen and (max-width: 720px) {
	.dashboard-page .stat > h1,
	.dashboard-page .stat > span {
		display: block;
		text-align: center;
		margin: 0;
		float: none;
		width: 100%;
	}
}

.dashboard-page .stat > span i {
	font-size: 0.7em;
}

.dashboard-page .stat img {
	pointer-events: none;
	position: absolute;
	left: 25px;
	width: calc(100% - 50px);
	bottom: 30px;
}

.dashboard-page .no-patient {
	display: block;
	text-align: center;
	padding: 30px;
	color: <?=$primaryColor?>;
	background-color: #fff;
}

.p-image {
	width: 40px;
	height: 40px;
	background-repeat: no-repeat;
	background-size: cover;
	border-radius: 100%;
}

#slider {
	border-radius: 5px;
	overflow: hidden;
	width: calc(50% - 12.5px);
	height: 170px;
	float: right;
	box-shadow: 10px 10px 20px -10px rgba(0, 0, 0, 0.1);
}

@media only screen and (max-width: 720px) {
	#slider {
		display: none !important;
	}
}

#slider figure {
  position: relative;
  width: 500%;
  margin: 0;
  left: 0;
  text-align: left;
  font-size: 0;
  animation: 30s slide infinite;
  height: 100%;
}

#slider figure img {
	width: 20%;
	float: left;
	height: 100%;
	object-fit: cover;
}

@keyframes slide {
	0% { left: 0%; }
	20% { left: 0%; }
	25% { left: -100%; }
	45% { left: -100%; }
	50% { left: -200%; }
	70% { left: -200%; }
	75% { left: -300%; }
	95% { left: -300%; }
	100% { left: -400%; }
}
</style>
