<?php include '../includes/variable.php'; ?>

<div class="header-connected-container">
    <div class="form">
        <input type="text" style="border-bottom: none; margin: 0" name="search-patient" placeholder="Search either first name, last name or middle name" />
        <i class="far fa-search icon right"></i>
    </div>

    <div class="tab-container four">
        <a href="#" class="item active order-by" o-value="id">Date Added</a>
        <a href="#" class="item order-by" o-value="lname">Last Name</a>
        <a href="#" class="item order-by" o-value="fname">First Name</a>
        <a href="#" class="item sort-by" s-value="DESC">Descending <i class="far fa-sort-amount-up"></i></a>
    </div>
</div>

<div class="table-container" style="margin-top: 285px; padding: 0; background-color: transparent;">
    <div class="patient-list"></div>
</div>

<a href="#" class="load-more-patients button solid button-separator" style="margin-bottom: 15px;"><i class="far fa-arrow-down"></i> Load More</a>

<script>
    setTimeout(function() {
    	global.setInfo('patients', true);
        $('.page-heading').append('<a class="button" href="#" pin="add-patient"><i class="far fa-user-plus"></i> Add Patient</a>');
        global.patientViewLimit = 15;
        global.patientViewOffset = 0;
        localStorage.setItem('pLoaded', 0);

        setTimeout(function(){
            global.reorderPatientList('id', 'DESC');
        }, 100);
    }, 1000);
</script>
