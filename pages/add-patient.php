<?php include '../includes/variable.php'; ?>

<div class="form">
	<div class="form-container" style="margin-bottom: 25px;">
		<form id="add-patient" date="<?php echo date("Y-m-d"); ?>">
			<h2 class="form-title">Full Name</h2>
			<input type="text" name="lname" placeholder="Last Name" />
			<input type="text" name="fname" placeholder="First Name" />
			<input type="text" name="mname" placeholder="Middle Name" />

			<h2 class="form-title">Birth Date</h2>
			<div class="date-container birth-date required"></div>


			<h2 class="form-title">Contact Information</h2>
			<input type="text" name="contactno" placeholder="Contact Number" />

			<input type="text" name="address" placeholder="Address" />


			<h2 class="form-title">Past Medical History</h2>
			<div class="past-med-history checklist">
				<a href="#" class="item" data-value="hypertension" check="false"><i class="far fa-square"></i> Hypertension</a>
				<a href="#" class="item" data-value="diabetes-mellitus" check="false"><i class="far fa-square"></i> Diabetes Mellitus</a>
				<a href="#" class="item" data-value="bronchial-asthma" check="false"><i class="far fa-square"></i> Bronchial Asthma</a>
				<div class="item-below hidden">
					<div class="date-container bronchial-asthma-date"></div>
				</div>
				<a href="#" class="item" data-value="surgery" check="false"><i class="far fa-square"></i> Surgery</a>
				<div class="item-below hidden">
					<div class="item-list surgery-list" placeholders="Enter surgery item"></div>
				</div>
			</div>


			<h2 class="form-title">Last Menstrual History</h2>
			<div class="date-container last-menstrual-date"></div>


			<h2 class="form-title">Age of Gestation</h2>
			<input type="text" name="age-gestation" placeholder="Age of Gestation" />


			<h2 class="form-title">OB History</h2>
			<div class="ob-history-field" style="position: relative">
				<div class="selection-list closable data-type numbers-collection ob-history-gravida" limit="13">
				    <span><span class="main-value">Gravida</span><i class="far fa-angle-down"></i></span>
				    <img class="carret" />
				    <div class="list" style="width: 100px; height: 140px;">
				        <div class="overflow"></div>
				    </div>
				</div>

				<div class="selection-list closable data-type numbers-collection ob-history-para" limit="13">
					<span><span class="main-value">Para</span><i class="far fa-angle-down"></i></span>
					<img class="carret" />
					<div class="list" style="width: 100px; height: 140px;">
						<div class="overflow"></div>
					</div>
				</div>

				<div class="ob-history-spaces">
					<div class="selection-list closable data-type numbers-collection" field-count="1" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="selection-list closable data-type numbers-collection" field-count="2" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="selection-list closable data-type numbers-collection" field-count="3" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="selection-list closable data-type numbers-collection" field-count="4" limit="100">
					    <span><span class="main-value">Select #</span><i class="far fa-angle-down"></i></span>
					    <img class="carret" />
					    <div class="list" style="width: 100px; height: 140px;">
					        <div class="overflow"></div>
					    </div>
					</div>

					<div class="clear"></div>
				</div>

				<a href="#" class="clear-field"><i class="far fa-eraser"></i> Clear</a>
			</div>


			<h2 class="form-title">Chief Complaint</h2>
			<div class="item-list chief-complaint-list" placeholders="Enter complaint item"></div>


			<h2 class="form-title">Physical Exam</h2>
			<input type="text" name="pe-bp" placeholder="BP" />

			<input type="text" name="pe-cr" placeholder="CR" />

			<input type="text" name="pe-temp" placeholder="Temp" />

			<div class="item-sub abdomen">
				<h3>Abdomen</h3>

				<input type="text" name="pe-abdomen-fh" placeholder="FH" />

				<input type="text" name="pe-abdomen-fht" placeholder="Fht" />
			</div>

			<input type="text" name="pe-ie" placeholder="IE" />

			<input type="text" name="pe-utz" placeholder="UTZ" />

			<input type="text" name="pe-others" placeholder="Others" />


			<h2 class="form-title">Lab Results</h2>
			<div class="item-list lab-result-list" placeholders="Enter lab result item"></div>


			<h2 class="form-title">Assessment</h2>

			<div class="item-list assessment-list" placeholders="Enter assessment item"></div>


			<h2 class="form-title">Plan</h2>

			<div class="item-list plan-list" placeholders="Enter plan item"></div>


			<h2 class="form-title">Follow Up</h2>
			<div class="date-container followup-date advanced"></div>

			<div class="item-list followup-instruction-list" placeholders="Enter follow up instruction item"></div>


			<input type="submit" name="submit" value="Add" />
		</form>
	</div>
</div>

<script type="text/javascript">
	setTimeout(function() {
		// set start data
		global.setInfo('add patient', true);
		$('a[pin="patients"], a[pin="add-patient"]').addClass('active');
	    $('.page-heading').append('<a class="button" href="#" pin="patients"><i class="far fa-arrow-left"></i> Back</a>');

		global.loadDatePicker();
		global.loadNumberCollections();
		global.loadItemList();
	}, 1000);
</script>
