<?php
	include 'includes/head.php';
	require $root . 'includes/db.php';
	session_start();

	// If form submitted, insert values into the database.
	if (isset($_POST['username'])){
	    // removes backslashes
		$username = stripslashes($_REQUEST['username']);

	    //escapes special characters in a string
		$username = mysqli_real_escape_string($con,$username);
		$password = stripslashes($_REQUEST['password']);
		$password = mysqli_real_escape_string($con,$password);

		//Checking is user existing in the database or not
	    $query = "SELECT * FROM `users` WHERE username='$username' and password='".md5($password)."'";
		$result = mysqli_query($con,$query) or die(mysql_error());
		$rows = mysqli_num_rows($result);
		$line = mysqli_fetch_array($result);
		$id = $line['id'];

	    $con->close();

	    if ($rows==1) {
		    $_SESSION['username'] = $username;
		    $_SESSION['id'] = $id;

	        // Redirect user to index.php
		    header("Location: /");
	     } else {
			echo "<div class='form'>
			<h3>Username/password is incorrect.</h3>
			<br/>Click here to <a href='login.php'>Login</a></div>";
		}
	} else {
?>

<div class="form login-form">
	<h1>Please sign in to your account</h1>

	<form action="" method="post" name="login">
		<input type="text" name="username" placeholder="Username" required />
		<input type="password" name="password" placeholder="Password" required />
		<input name="submit" type="submit" value="Login" />
	</form>

	<p class="hidden">Not registered yet? <a href='/registration'>Register Here</a></p>
</div>

<script>
$(window).on('load', function() {
	global.setInfo('login', false);
    localStorage.removeItem('currentPage');
    localStorage.removeItem('pID');
    localStorage.removeItem('pLoaded');
    localStorage.removeItem('pulledNotif');
    localStorage.removeItem('pulledSchedule');
    localStorage.removeItem('pulledMessages');
    localStorage.removeItem('convoID');
	localStorage.removeItem('audioAllowedDashboard');
});
</script>

<style>
.login-page {
	background-image: url('/public/system/images/login-background.jpg');
	background-size: 620px;
	background-repeat: no-repeat;
	background-position: top right;
	background-color: #e5e5e5;
}

.login-form {
	width: 100%;
	max-width: 350px;
	top: 50%;
	position: fixed;
	left: 50%;
	transform: translate(-50%, -50%);
	background-color: #fbfbfb;
	border-radius: 5px;
	box-shadow: 10px 10px 20px -10px rgba(0, 0, 0, 0.1);
}
</style>

<?php
	};
	include $root . 'includes/footer.php';
?>
