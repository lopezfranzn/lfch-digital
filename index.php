<?php
    include 'includes/variable.php';
    include 'includes/auth.php';
    include 'includes/head.php';
    include 'includes/element/side-panel.php';
    include 'includes/element/main-content.php';
    include 'includes/element/modal.php';
    include 'includes/element/notification.php';
    include 'includes/footer.php';
?>

<script>
    $(window).on('load', function () {
        global.goToMain();
    });
</script>
