-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2018 at 11:26 AM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+08:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lfchdigital`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` longtext NOT NULL,
  `fname` longtext NOT NULL,
  `lname` longtext NOT NULL,
  `title` longtext NOT NULL,
  `image` longtext NOT NULL,
  `email` longtext NOT NULL,
  `atype` longtext NOT NULL,
  `password` longtext NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Table structure for table `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL,
  `image` longtext NOT NULL,
  `lname` longtext NOT NULL,
  `fname` longtext NOT NULL,
  `mname` longtext NOT NULL,
  `birth_date` longtext NOT NULL,
  `contact_no` longtext NOT NULL,
  `address` longtext NOT NULL,
  `past_med_history` longtext NOT NULL,
  `last_mens_history` longtext NOT NULL,
  `age_gestation` longtext NOT NULL,
  `ob_history` longtext NOT NULL,
  `chief_complaint` longtext NOT NULL,
  `physical_exam` longtext NOT NULL,
  `lab_result` longtext NOT NULL,
  `assessment` longtext NOT NULL,
  `plan` longtext NOT NULL,
  `follow_up` longtext NOT NULL,
  `follow_up_status` longtext NOT NULL,
  `trn_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL,
  `mfrom` longtext NOT NULL,
  `mto` longtext NOT NULL,
  `content` longtext NOT NULL,
  `status` longtext NOT NULL,
  `trn_date` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL,
  `nfrom` longtext NOT NULL,
  `target` longtext NOT NULL,
  `content` longtext NOT NULL,
  `status` longtext NOT NULL,
  `trn_date` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Table structure for table `med_records`
--

CREATE TABLE IF NOT EXISTS `med_records` (
  `id` int(11) NOT NULL,
  `pid` longtext NOT NULL,
  `rec_type` longtext NOT NULL,
  `content` longtext NOT NULL,
  `trn_date` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fname`, `lname`, `title`, `image`, `email`, `atype`, `password`, `trn_date`) VALUES
(1, 'lopezfranzn', 'Franz', 'Lopez', 'Developer', '1', 'lopezfranzn@gmail.com', 'sadmin', 'e10adc3949ba59abbe56e057f20f883e', '2018-10-22 10:18:37'),
(2, 'maclopez', 'Maricel', 'Lopez', 'OB/GYN', '2', 'N/A', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2018-11-04 16:14:00'),
(3, 'ltiburcio', 'Lea', 'Tiburcio', 'Secretary', '3', 'N/A', 'standard', 'e10adc3949ba59abbe56e057f20f883e', '2018-11-04 16:31:00');

--
-- Dumping data for table `patients`
--

-- INSERT INTO `patients` (`id`, `image`, `lname`, `fname`, `mname`, `birth_date`, `contact_no`, `address`, `past_med_history`, `last_mens_history`, `age_gestation`, `ob_history`, `chief_complaint`, `physical_exam`, `lab_result`, `assessment`, `plan`, `follow_up`, `follow_up_status`, `trn_date`) VALUES
-- (1, '0', 'Doe', 'Jane', 'Berry', '1992-10-10', '09294330483', '445 Mount Eden Road, Mount Eden, Auckland', '[{"hypertension":"true","diabetesMellitus":"false","bronchialAsthma":{"value":"true","date":"10-10-1998"},"surgery":{"value":"true","list":["Appendix removal","Normal delivery"]}}]', '2000-10-10', '38 4/7 weeks', '[{"gravida":"2","para":"1","digits":"2-0-0-1"}]', '[{"list":["Complaint item 1","Complaint item 2"]}]', '[{"bp":"180/80","cr":"10.4","temp":"38.5","abdomen":{"fh":"2","fht":"1"},"ie":"2","utz":"3","others":"Sample test","stamp":"2018-10-22"},{"bp":"180/80","cr":"10.4","temp":"38.5","abdomen":{"fh":"2","fht":"1"},"ie":"2","utz":"3","others":"Sample test","stamp":"2018-10-22"}]', '[{"list":["Lab result item 1","Lab result item 2"],"stamp":"2018-10-22"}]', '[{"list":["Assessment item 1","Assessment item 2"],"stamp":"2018-10-22"}]', '[{"list":["Plan item 1","Plan item 2"]}]', '[{"date":"2011-10-10","list":["Follow up instruction item 1","Follow up instruction item 2"]}]', 'false', '2018-10-22');

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `mfrom`, `mto`, `content`, `status`, `trn_date`) VALUES
(1, '2', '3', 'Hi secretary! This is just a test message.', 'unread', 'November 16, 2018 7:46 PM'),
(3, '3', '2', 'Copy doc.', 'unread', 'November 16, 2018 7:48 PM');

--
-- Dumping data for table `notifications`
--

-- INSERT INTO `notifications` (`id`, `nfrom`, `target`, `content`, `status`, `trn_date`) VALUES
-- (1, '3', 'admin', 'added a new patient.', 'unread', 'November 16, 2018 7:46 PM'),
-- (2, '3', 'admin', 'deleted a patient.', 'unread', 'November 16, 2018 7:55 PM'),
-- (3, '2', 'standard', 'edited a patient.', 'unread', 'November 16, 2018 8:55 PM');

--
-- Dumping data for table `med_records`
--

-- INSERT INTO `med_records` (`id`, `pid`, `rec_type`, `content`, `trn_date`) VALUES
-- (1, '1', 'pe', '[{"bp":"180/80","cr":"10.4","temp":"38.5","abdomen":{"fh":"2","fht":"1"},"ie":"2","utz":"3","others":"Sample test"}]', '2018-10-22'),
-- (2, '1', 'lr', '["Lab result item 1","Lab result item 2"]', '2018-10-22'),
-- (3, '1', 'at', '["Assessment item 1","Assessment item 2"]', '2018-10-22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `med_records`
--
ALTER TABLE `med_records`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `med_records`
--
ALTER TABLE `med_records`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
